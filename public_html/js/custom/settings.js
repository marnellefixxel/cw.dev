"use strict"
var base_url = $('html').attr('base-url');

$(document).ready(function() 
{
    $('#show_subcat').click(function(){
      $( "#add_subCat" ).toggle( "slow", function() {
      });
    });

    //display sub category based on the selected category
    $('#job_category_id').change(function(){
      var jobcat_id = $('#job_category_id').val();
      var process_url = base_url + 'admin/settings/GET_SUBCATEGORY';

        if(jobcat_id.length > 0)
        {

          $('#disp').show().append('<i class="fa fa-spinner fa-spin"></i> fetching data...');

          $.post(process_url, {jobcat_id:jobcat_id}, function(result){
            try {
                var result = $.parseJSON(result);
                var theresult = "";

                if(result.subcat_list.length > 0)
                {
                  for (var i=0; i<result.subcat_list.length; i++)
                  {
                    theresult += '<option value='+result.subcat_list[i].id+'>'+result.subcat_list[i].job_type_name+'</option>';
                    $('#sub_category').empty().append(theresult);
                  }
                }
            }
            catch(e) {
                console.log(result);
            }
          })
          .fail(function(test) {
            alert(test.responseText);
          });

          setTimeout(function(){
            $('#disp').empty().hide().fadeIn();
          },2000);

        }
    });


    // ajax pagination in supplier
    $('#supplier').on('click','.pagination li > a',function(e){
      var url = $(this).attr('href');
        if(url == null){
          return false;
        }
        $.get(url, function(data){
          var html_element = $(data).find('#table-container');
          $("#supplier").html(html_element);
        });
      e.preventDefault();
    });

    // ajax pagination in job category
    $('#jo_order_cat').on('click','.pagination li > a',function(e){
      var url = $(this).attr('href');
        if(url == null){
          return false;
        }
        $.get(url, function(data){
          var html_element = $(data).find('#table-container1');
          $("#jo_order_cat").html(html_element);
        });
      e.preventDefault();
    });

    // ajax pagination in job classification
    $('#job_classification').on('click','.pagination li > a',function(e){
      var url = $(this).attr('href');
        if(url == null){
          return false;
        }
        $.get(url, function(data){
          var html_element = $(data).find('#table-container2');
          $("#job_classification").html(html_element);
        });
      e.preventDefault();
    });

}); 
// end of doc ready

// adding new supplier
function ADD_NEW_SUPPLIER()
{
  var supp_name         = $('#supp_name').val();
  var supp_contact_name = $('#supp_contact_name').val();
  var supp_contact_num  = $('#supp_contact_num').val();
  var supp_email        = $('#supp_email').val();

  var post_url          = base_url + 'admin/settings/ADD_SUPPLIER';

  if(supp_name.length > 0)
  {
    $('#add_supplier_btn').show( function(){
      $('#add_supplier_btn').empty().append('<i class="fa fa-spinner fa-spin"></i> saving...');
    });
    
    $.post(post_url, { supp_name:supp_name, supp_contact_name:supp_contact_name, supp_contact_num:supp_contact_num, supp_email:supp_email }, function(result){
      var result = $.parseJSON(result);

      if(result.stat == true)
      {
          bootbox.alert('New Supplier successfully added');
         $('#supplier_table').append('<tr><td class="text-center">'+result.content.id+'</td>'+
          '<td>'+result.content.supp_co_name+'</td><td>'+result.content.supp_contact_person+'</td>'+
          '<td>'+result.content.supp_contact_num+'</td><td>'+result.content.supp_email+'</td>'+
          '<td class="text-center"><a href=""><i class="fa fa-edit"></i></a></td>'+
          '<td class="text-center"><a href=""><i class="fa fa-times"></i></a></td>'+
          '</tr>');
      }
      else
      {
        alert('Error saving new supplier');
      }
      // window.location.reload();
    });

    setTimeout(function(){
      $('#add_supplier_btn').empty().hide().fadeIn().append('Add');
    },3000);
  }
  else
  {
    alert('Please enter supplier name');
    return false;
  }
}

// adding new job category
function ADD_JOB_CATEGORY()
{
  var job_name = $('#category_name').val();
  var job_desc = $('#cat_description').val();
  var cat_stat = $('.cat_stat:checked').val();
  var post_url = base_url + 'admin/settings/ADD_JOB_ORDER';

  var subcat_array = new Object;
  var tm_arr = [];

  $(".subcat_list ul li span.thesubcat").each(function(){ 
    tm_arr.push($(this).html());
  });

  subcat_array.subCategory = tm_arr;
  subcat_array.job_name   = job_name;
  subcat_array.job_desc   = job_desc;
  subcat_array.cat_stat   = cat_stat;

  // console.log(subcat_array);

  if(job_name.length > 0)
  {

    $('#add_jobcat_btn').show( function(){
      $('#add_jobcat_btn').empty().append('<i class="fa fa-spinner fa-spin"></i> saving...');
    });

    $.post(post_url, subcat_array, function(result){
      
      try {
        var result = $.parseJSON(result);
        alert(result.msg);
        window.location.reload();
      } 
      catch(e) {
        console.log(result);
      }
    })
    .fail(function(test){
      alert(test.responseText);
    });

    setTimeout(function(){
      $('#add_jobcat_btn').empty().hide().fadeIn().append('Add');
    },3000);
  }

  else
  {
    return false;
  }
}

// adding new job classification
function ADD_JOB_CLASSIFICATION()
{
  var job_class_name  = $('#job_class_name').val();
  var tbl_supplier_id = $('#tbl_supplier_id').val();
  var job_category_id = $('#job_category_id').val();
  var sub_category    = $('#sub_category').val();
  var job_class_desc  = $('#job_class_desc').val();

  var post_url = base_url + 'admin/settings/ADD_JOB_CLASSIFICATION';

  if(job_class_name.length > 0)
  {

    $('#add_jobclass_btn').show( function(){
      $('#add_jobclass_btn').empty().append('<i class="fa fa-spinner fa-spin"></i> saving...');
    });

    $.post(post_url, {job_class_name:job_class_name, tbl_supplier_id:tbl_supplier_id, job_category_id:job_category_id, sub_category:sub_category, job_class_desc:job_class_desc}, function(result){

      try {
        var result = $.parseJSON(result);
        alert(result.msg);
        window.location.reload();
      } 
      catch(e) {
        console.log(result);
      }
    })
    .fail(function(test){
      alert(test.responseText);
    });

    setTimeout(function(){
    $('#add_jobclass_btn').empty().hide().fadeIn().append('Add');
    },3000);
  }

  else
  {
    return false;
  }
}


// adding new sub category
function get_subcat(e)
{
    var subCat_name = $('#subCat_name').val();
    if (e.keyCode == 13)
    {
      if(subCat_name.length == "")
      {
        return false;
      }

      var thedata = '<li onclick="remove(this);"style="cursor:pointer;font-size:11px; margin-right:5px;" class="label label-success"><span class="thesubcat">'+subCat_name+'</span>&nbsp;&nbsp;<i class="fa fa-times"></i></li>';
      $(".subcat_list ul").append(thedata);
      $('#subCat_name').val('');
    }
}

