'use strict'
var app = angular.module('cw',[], function($interpolateProvider) {
   
});
app.run(['$rootScope','$sce',function($rs,sce){
	
	$rs.base_url = $.trim($('html').attr('base-url')); //set the base url
	$rs.dialogContent      = "";
	$rs.dialogColor        = "";
	$rs.customerTabColor   = "";
	$rs.customerTabContent = "";
	$rs.timeOutHandler     = "";
	
	//when the change starts
	$rs.$on('$stateChangeStart',function(event, toState, toParams, fromState, fromParams){
		$rs.dialogContent = "";
		$rs.dialogColor   = "";
	});

	//when the change succeeds
	$rs.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){
		
	});

	//when the change state encounters an error
	$rs.$on('$stateChangeError',function(event, toState, toParams, fromState, fromParams, error){ 
	   
	});
	
/*	//count the timer
	$rs.$watch('customerTabColor',function(){
		clearTimeout($rs.timeOutHandler);
		$rs.timeOutHandler  = setTimeout(function(){
			$rs.$apply(function(){
				$rs.customerTabColor   = sce.trustAsHtml("");
				$rs.customerTabContent = sce.trustAsHtml("");
			});
		},5000);
	});
	
	//count the timer
	$rs.$watch('dialogColor',function(){
		clearTimeout($rs.timeOutHandler);
		$rs.timeOutHandler = setTimeout(function(){
			$rs.$apply(function(){
				$rs.dialogContent = sce.trustAsHtml("");
				$rs.dialogColor   = sce.trustAsHtml("");
			});
		},5000);
	});*/
}]);