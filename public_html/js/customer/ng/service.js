'use strict'
var app = angular.module('cw');

app
.factory('Ajax',['$rootScope','$http',function($rs,$ht){
	
	var fac = {};
	
	fac.restAction = function(obj){
		
		var ht = $ht
				(
					{
						method:obj.method,
						url   :obj.url,
						data  :obj.data,
						params:obj.params,
						transaformRequest:angular.identity,
						headers:{'Content-Type':undefined}
					}
				);

		return ht;
	}
	
	return fac;
	
}])
.factory('Map',['$rootScope','$http',function($rs,$ht){
	var fac = {};

	fac.geocode = function(obj){
		var ht  = $ht(
						{
							method : "GET",
							url    : "https://maps.googleapis.com/maps/api/geocode/json",
							params : obj.params,
							transaformRequest : angular.identity,
							headers : {'Content-Type':undefined}
						}
					);
		return ht;
	}

	return fac;
}]);