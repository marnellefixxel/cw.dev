<?php $this->load->view('backend/components/backend_header'); ?>
    <div ng-cloak class="alert alert-{{dialogColor}} mainContainer-alert" ng-bind-html="dialogContent" ng-if="dialogColor.length!==0 && dialogContent.length!==0"></div>
    <?php 
        if(is_array($subview)) {
            foreach ($subview as $subview_child)  {
                $this->load->view($subview_child);
            }
        } else {
            $this->load->view($subview);
        }
    ?>
<?php $this->load->view('backend/components/backend_footer'); ?>