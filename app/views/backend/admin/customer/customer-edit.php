<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDZlWBe7yGIkjC8Izq4VpPFDtFNsgIZKQs"></script> <!-- remote -->
<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Customer</a>
        </li>
    </ul>
</div>
<div class="box-inner">
    <div class="box-header well">
        <h2><i class="glyphicon glyphicon-user"></i> Customer Area</h2>
        <div class="box-icon">
            <!-- <a class="btn btn-setting btn-round btn-default" href="#"><i class="glyphicon glyphicon-cog"></i></a> -->
            <a class="btn btn-minimize btn-round btn-default" href="#"><i class="glyphicon glyphicon-chevron-up"></i></a>
            <!-- <a class="btn btn-close btn-round btn-default" href="#"><i class="glyphicon glyphicon-remove"></i></a> -->
        </div>
    </div>
    <div class="box-content">
        <div ng-controller="CustomerEdit"  customerid="<?php echo $customerid; ?>" ng-cloak=""> 

            <!--  registration form -->
            <div class='customer-registration' ng-show="!loading">
                <!--  header -->
                <div class="clearfix header">
                    <div class="col-xs-6 rm-padding">
                        <h4>
                            <span ng-if="customer.id===0">
                                REGISTRATION
                            </span>
                            <span ng-if="customer.id!==0">
                                {{customer.cus_salutations}} {{customer.cus_name}}
                            </span>
                        </h4>
                    </div>
                    <div class="col-xs-6 text-right rm-padding">
                        <button class="btn btn-primary btn-save" ng-click="saveCustomer();">
                            SAVE
                        </button>
                    </div>
                </div>
                <!--  /.  -->
                
                <!--  form  -->
                <form  class="horizontal-form" id="customer-registration-form">
                    <input type="text" style='display:none;' name="customer_id"  ng-model="customer.id">
                    
                    <!--  main information -->
                    <div class="registration-sections">
                        <div class="clearfix item-input">
                            <div class="col-xs-2 col-sm-2 customer-input-hs">
                                <label>
                                    <i class="fa fa-flag"></i>
                                </label>
                                <select class="form-control" name="cus_salutations" ng-model="customer.cus_salutations">
                                    <option value="">-</option>
                                    <option value="Mr.">Mr.</option>
                                    <option value="Mrs.">Mrs.</option>
                                    <option value="Ms.">Ms.</option>
                                    <option value="Mdm.">Mdm.</option>
                                    <option value="Dr.">Dr.</option>
                                </select>
                            </div>
                            <div class="col-xs-2 col-sm-4 customer-input-hs">
                                <label>
                                    Role
                                </label>
                                <select name="cus_role" id="" class="form-control" ng-model="customer.cus_role">
                                    <option value="">-</option>
                                    <option value="Owner">Owner</option>
                                    <option value="Agent">Agent</option>
                                    <option value="Staff">Staff</option>
                                    <option value="Helper">Helper</option>
                                    <option value="Family">Family</option>
                                </select>
                            </div>
                            <div class="col-sm-6 col-xs-8 customer-input-hs">
                                <label for="cusName">Customer Name*</label>
                                <input type="text" name="cus_name" ng-blur="checkCustomerDuplicity();" class="form-control" ng-model="customer.cus_name" placeholder="Name of the customer">
                                <span class="label label-warning" ng-show="duplicates.length!==0">
                                    {{duplicates.length}} possible duplicate(s).
                                    <a href="javascript:void(0);" onclick="$('#toggle_possible_duplicates').toggle();">
                                        Toggle
                                    </a>
                                </span>
                            </div>
                        </div>
                        
                        <!--  possible duplicates -->
                        <div class="clearfix item-input border-dashed" id="toggle_possible_duplicates" style='display:none;'>
                            <div class="col-sm-12">
                                <h5>
                                    <i class="fa fa-exclamation"></i>
                                    &nbsp;POSSIBLE DUPLICATES ({{duplicates.length}})
                                </h5>
                                <hr>
                                <div>
                                    <span class="label label-info duplicate-label" ng-repeat="duplicate in duplicates" ng-click="redirectCustomer(duplicate.id);">{{duplicate.cus_name}}</span>
                                </div>
                            </div>
                        </div>
                        <!--  /.  -->
                        
                        <!--  contact details -->
                        <div class="clearfix item-input border-dashed">
                            <div class="col-sm-12">
                                <h5>
                                    <i class="fa fa-phone"></i>
                                    &nbsp;CONTACT DETAILS
                                </h5>
                                <hr>
                            </div>
                        </div>
                        <div class="clearfix item-input">
                            <div class="col-sm-4 customer-input-hs-hof">
                                <label class="clearfix">
                                    <div class="pull-left">
                                        Home No.
                                    </div>
                                    <div class="pull-right">
                                        <a href="javascript:void(0);" title="Add more home numbers" ng-click="customer.cus_hom_num.push('');" class=' btn btn-default btn-xs'>
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </label>
                                <div class='multiple-items' ng-repeat="con in customer.cus_hom_num track by $index">
                                    <span class="float-number">{{$index+1}}.</span>
                                    <input type="text" class="form-control" placeholder="Enter No." name="cus_hom_num[]" ng-model="customer.cus_hom_num[$index]" />
                                    <a href="javascript:void(0);" title="Remove home number" class='btn btn-danger btn-xs' ng-show="$index!==0" ng-click="customer.cus_hom_num.splice($index,1);">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-4 customer-input-hs-hof">
                                <label class="clearfix">
                                    <div class="pull-left">
                                        Office No.
                                    </div>
                                    <div class="pull-right">
                                        <a href="javascript:void(0);" title="Add more office numbers" ng-click="customer.cus_off_num.push('');" class=' btn btn-default btn-xs'>
                                           <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </label>
                                <div class='multiple-items' ng-repeat="con in customer.cus_off_num track by $index">
                                    <span class="float-number">{{$index+1}}.</span>
                                    <input type="text" class="form-control" placeholder="Enter No." name="cus_off_num[]" ng-model="customer.cus_off_num[$index]" >
                                    <a href="javascript:void(0);" title="remove office number" class='btn btn-danger btn-xs' ng-show="$index!==0"  ng-click="customer.cus_off_num.splice($index,1);">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-4 customer-input-hs-hof">
                                <label class="clearfix">
                                    <div class="pull-left">
                                        Fax No.
                                    </div>
                                    <div class="pull-right">
                                        <a href="javascript:void(0);"  title="Add more fax numbers" ng-click="customer.cus_fax_num.push('');" class=' btn btn-default btn-xs'>
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </label>
                                <div class='multiple-items' ng-repeat="con in customer.cus_fax_num track by $index">
                                    <span class="float-number">{{$index+1}}.</span>
                                    <input type="text" class="form-control" placeholder="Enter No." name="cus_fax_num[]" ng-model="customer.cus_fax_num[$index]" >
                                    <a href="javascript:void(0);" title="Remove fax number" class='btn btn-danger btn-xs' ng-show="$index!==0"  ng-click="customer.cus_fax_num.splice($index,1);">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix item-input">
                            <div class="col-sm-6 customer-input-hs-hpe">
                                <label class="clearfix">
                                    <div class="pull-left">
                                       Hand Phone
                                    </div>
                                    <div class="pull-right">
                                        <a href="javascript:void(0);" title="Add more hand phones" ng-click="customer.cus_han_num.push('');" class=' btn btn-default btn-xs'>
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </label>
                                <div class='multiple-items' ng-repeat="con in customer.cus_han_num track by $index">
                                    <span class="float-number">{{$index+1}}.</span>
                                    <input type="text" class="form-control" placeholder="Enter No." name="cus_han_num[]" ng-model="customer.cus_han_num[$index]" >
                                    <a href="javascript:void(0);" title="Remove hand phone" class='btn btn-danger btn-xs' ng-show="$index!==0"  ng-click="customer.cus_han_num.splice($index,1);">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-6 customer-input-hs-hpe">
                                <label class="clearfix">
                                    <div class="pull-left">
                                       Email
                                    </div>
                                    <div class="pull-right">
                                        <a href="javascript:void(0);" title="Add more emails" ng-click="customer.cus_email.push('');" class=' btn btn-default btn-xs'>
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </label>
                                <div class='multiple-items' ng-repeat="con in customer.cus_email track by $index">
                                    <span class="float-number">{{$index+1}}.</span>
                                    <input type="text" class="form-control" placeholder="Enter Email" name="cus_email[]" ng-model="customer.cus_email[$index]" >
                                    <a href="javascript:void(0);" title="Remove email" class='btn btn-danger btn-xs' ng-show="$index!==0"  ng-click="customer.cus_email.splice($index,1);">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!--  /.  -->
                    </div>
                    <!--  /.  -->
                    
                    <!--  mailing address -->
                    <div class="registration-sections">
                        <div class="clearfix item-input border-dashed">
                            <div class="col-sm-12">
                                <h5>
                                    <i class="fa fa-truck"></i>
                                    &nbsp;MAILING ADDRESS
                                </h5>
                                <hr>
                            </div>
                        </div>
                        <div class="clearfix item-input">
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <label>Blk #</label>
                                        <input type="text"  ng-model="customer.cus_address_blk" class="form-control" name="cus_blk" placeholder="Enter Blk">
                                    </div>
                                    <div class="col-xs-6">
                                        <label>Unit #</label>
                                        <input type="text"  ng-model="customer.cus_address_unit" class="form-control" name="cus_unit" placeholder="Enter Unit #">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <label>Street Name</label>
                                        <input type="text"  ng-model="customer.cus_address_street" class="form-control" name="cus_street" placeholder="Enter Street Name">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <label>Building Name</label>
                                        <input type="text"  ng-model="customer.cus_address_bldg" class="form-control" name="cus_bldg" placeholder="Enter Building Name">
                                    </div>
                                    <div class="col-xs-6">
                                        <label>Postal</label>
                                        <input type="text" ng-blur="mapLoader();" ng-model="customer.cus_address_postal" class="form-control" name="cus_postal" placeholder="Enter Postal">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="map-container" id="map-container"></div>
                            </div>
                        </div>
                    </div>
                    <!--  /.  -->
                    
                    <!--  remarks -->
                    <div class="registration-sections">
                        <div class="clearfix item-input border-dashed">
                            <div class="col-sm-12">
                                <h5>
                                    <i class="fa fa-comment"></i>
                                    &nbsp;REMARKS
                                </h5>
                                <hr>
                            </div>
                        </div>
                        <div class="clearfix item-input">
                            <div class="col-sm-12">
                                <textarea name="cus_remarks" ng-bind="customer.cus_remark" cols="30" rows="3" class="form-control" placeholder="Enter remarks"></textarea>
                            </div>
                        </div>
                    </div>
                    <!--  /.  -->
                </form>
                <!--  /.  -->
            </div> 
            <!--  /.  -->
                
            <div class="customer-registration" ng-show="loading">
                <div class="registration-sections">
                    <div class="general-empty text-center">
                        <i class="fa fa-user"></i>
                        <p>
                            Loading Section
                        </p>
                    </div>
                </div>
            </div>
            
            <!-- tab repudiation -->
            <div class="alert alert-{{customerTabColor}}" style="margin-bottom:5px;" ng-bind-html="customerTabContent" ng-if="customerTabColor.length!==0 && customerTabContent.length!==0">
            </div>
            <!-- /. -->
            
            <!--  customer tabs  -->
            <div role="tabpanel" class="customer-tabs" ng-show="customerId!=0 && !loading">
                
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#contact_persons"  role="tab" data-toggle="tab">
                            Contact Persons
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#properties" role="tab" data-toggle="tab">
                            Properties
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#transactions" role="tab" data-toggle="tab">
                            Transactions
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#logs" role="tab" data-toggle="tab">
                            Logs
                        </a>
                    </li>
                </ul>
                <!--  /.  -->
                
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane " id="properties">
                        <?php 
                            $this->load->view('backend/admin/customer/properties');
                        ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="transactions">
                        <?php 
                            $this->load->view('backend/admin/customer/transaction');
                        ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="logs">
                    </div>
                    <div role="tabpanel" class="tab-pane contact-person-tab active" id="contact_persons">
                        <form action="" method="POST" class="form-horizontal" role="form" id="customer-contact-person-form">
                            <input type="text" name="customerid" ng-model="customerId" style="display:none;">
                            <div class="clearfix">
                                <div class="col-sm-6 border-dash">
                                    <h5 class="pull-left">
                                        <i class="fa fa-link"></i>
                                        &nbsp;CONTACT PERSON(S)
                                    </h5>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <a href="javascript:void(0);" ng-click="addCustomerContact();" class=' btn btn-default'>
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <a href="javascript:void(0);" class="btn btn-primary" ng-click="saveContacts();">
                                        <i class="fa fa-save"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="clearfix border-dash">
                                <div class="col-sm-12">
                                    <label class="clearfix ul-notes">
                                        For multiple contacts, separate each number with a comma*
                                    </label>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-sm-12">
                                    <div class="well well-sm"  ng-repeat="contacts in customer.contact_persons track by $index">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <b>
                                                    Contact Person # {{$index+1}} 
                                                </b>
                                            </div>
                                            <div class="col-xs-6 text-right" ng-show="$index!==0">
                                                <a href="javascript:void(0);" class="btn btn-danger btn-xs" ng-click="customer.contact_persons.splice($index,1)">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <input type="text" name="contact_id[]" ng-model="customer.contact_persons[$index].id" style="display:none;">
                                            <div class="col-sm-3">
                                                <label>Name</label>
                                                <input type="text" class="form-control" name="contact_name[]" placeholder="Enter name" ng-model="customer.contact_persons[$index].name">
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Role</label>
                                                <input type="text" class="form-control" name="contact_role[]" placeholder="Enter Role" ng-model="customer.contact_persons[$index].role">
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Contact</label>
                                                <input type="text" class="form-control" name="contact_contact[]" placeholder="Enter contact" ng-model="customer.contact_persons[$index].contacts">
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Email</label>
                                                <input type="text" class="form-control" name="contact_email[]" placeholder="Enter email" ng-model="customer.contact_persons[$index].email">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--  /.  -->
            </div>
            <!--  /.  -->
        </div>
    </div>
</div>