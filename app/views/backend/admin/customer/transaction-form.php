<div class="transaction-form" style="padding: 20px;">
	<?php 
	    $thelogo = base_url('images/cottonware.png');
	    echo '<img class="img-responsive" src="'.$thelogo.'"/>';
	?>
		<hr>
		<div class="row">
			<div class="col-md-1">
				<h5>Attention:</h5>
			</div>
			<div class="col-md-2">
				<h5>_____________________________</h5>
			</div>	
			<div class="col-md-1 col-md-offset-5">
				<h5>Date:</h5>
			</div>
			<div class="col-md-1">
				<h5>_____________________________</h5>
			</div>	
		</div>
		<div class="row">
			<div class="col-md-1">
				<h5>Email:</h5>
			</div>
			<div class="col-md-2">
				<h5>_____________________________</h5>
			</div>	
			<div class="col-md-1 col-md-offset-5">
				<h5>Ref:</h5>
			</div>
			<div class="col-md-1">
				<h5>_____________________________</h5>
			</div>	
		</div>
		<div class="row">
			<div class="col-md-1">
				<h5>Address:</h5>
			</div>
			<div class="col-md-2">
				<h5>_____________________________</h5>
			</div>	
		</div>
		<div class="row">
			<div class="col-md-1">
				<h5></h5>
			</div>
			<div class="col-md-2">
				<h5>_____________________________</h5>
			</div>	
		</div>
		<div class="row">
			<div class="col-md-1">
				<h5>Contact:</h5>
			</div>
			<div class="col-md-2">
				<h5>_____________________________</h5>
			</div>		
		</div>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<h5>We are pleased to append herewith our quotation for your kind consideration.</h5>
			</div>
		</div>
		<div class="row">
			<table class="table-responsive table-hover table">
				<thead>
					<tr>
						<td><h4>Item</h4></td>
						<td><h4>Description</h4></td>
						<td><h4>Quantity</h4></td>
						<td><h4>Amount $</h4></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td></td>
						<td>To Supply and Install</td>
						<td></td>
						<td> </td>
					</tr>
					<tr>
						<td></td>
						<td><u>2nd Floor Maid Room</u><br>
							To Supply and Install Curtain Track (69.5")
						</td>
						<td><br> 1pc</td>
						<td><br> $ 30.00</td>
					</tr>
					<tr>
						<td> </td>
						<td><u>2nd Floor Daughter Room</u><br>
							To supply and Install Standard Double Pleated<br>
							Night Curtain Curtain Track (70")</td>
						<td><br> 
							1 set <br>
							1 pc</td>
						<td> <br>
							$ 300.00<br>
							$ 30.0</td>
					</tr>
					<tr>
						<td> </td>
						<td><u>2nd Floor Master Room</u><br>
							To Supply and Install Curtain Track(75")</td>
						<td><br> 1 pc</td>
						<td><br> $ 35.00</td>
					</tr>
					<tr>
						<td> </td>
						<td><u>Third Floor Walkway</u>
							<br> To Supply and Install
							<br> Standard Double Pleated Night Curtain
							<br> Curtain Track (16", 16", 67")
							<br> To Shorten Day Curtain to Height 84.5" (CC)</td>
						<td><br> 1 set
							<br> 3 pcs
							<br> 1 set</td>
						<td><br> $ 340.00
							<br> $ 40.00
							<br> $ 40.00</td>	
					</tr>
					<tr>
						<td> </td>
						<td> <u>Third Floor Daughter Room</u>
							<br> To Supply and Install
							<br> Curtain Track (16", 16" 112")
							<br> To Shorten Day curtain to Height 84.5" (CC)</td>
						<td> <br>
							3 pcs
							<br> 1 set</td>
						<td> <br>
							$ 60.00
							<br> $ 40.00</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td><strong>Total</strong></td>
						<td> <strong>$ 915.00</strong></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td><strong>Less Discount</strong></td>
						<td> <strong>$ 65.00</strong></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td><strong>Total</strong></td>
						<td> <strong>$ 850.00</strong></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="row">
			<div class="col-md-1">
				<h4><u>Remarks</u></h4>
			</div>	
		</div>
		<div class="row">
			<div class="col-md-12">
				<h5>1. Validity: 30 days from date of quotation.</h5>
			</div>	
		</div>
		<div class="row">
			<div class="col-md-12">
				<h5>2. Terms of Payment: Cash on delivery or cheque made payable to Cotton Ware LLP.</h5>
			</div>	
		</div>
		<div class="row">
			<div class="col-md-12">
				<h5>3. The above cost is not subjected to GST.</h5>
			</div>	
		</div>
		<div class="row">
			<div class="col-md-12">
				<h5>4. Deposit Required: 10%</h5>
			</div>	
		</div>
		<br>
		<br>
		<br>
		<i><h4>Lovely Agero</h4></i>
		<br>
		<br>
		<br>
		<br>
		<div class="row footer-form">
			<h3>Cotton Ware LLP</h3> <br>
			 53 Ubi Ave 1 Paya Ubi Industrial Park #01-29 Singapore 408934 <br>
			 Tel: 6743 4048   Fax: 6848 1149 Website: www.cottonware.com.sg <br> 
			 Email: enquire@cottonware.com.sg
		</div>
</div>
