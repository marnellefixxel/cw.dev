<?php 
	# check if the current view is in the settings page
    $checkpage = $this->uri->segment(2);
	$sub_page  = $this->uri->segment(3);

	if($checkpage == 'customer') {
		if($sub_page == '') {
			$this->load->view('backend/admin/customer/customer-list');
		}
		else if($sub_page == 'edit') {
			$this->load->view('backend/admin/customer/customer-edit');
		}
	}
?>

