<div class="row">
	<div class="col-md-12 clearfix">
		<div class="pull-right clearfix"><a href="<?php echo base_url('admin/joborder/edit'); ?>" target=blank class="btn btn-primary btn-xs pull-right">Create New Transaction</a></div>
	</div>
</div>
<div class="row">
	<div class="col-md-12 table_joborder" >
		<table class="table table-responsive table-hover">
			<thead>
				<tr>
					<td class="text-center"><h4>JO ID</h4></td>
					<td class="text-center"><h4>Property</h4></td>
					<td class="text-center"><h4>Transaction Date</h4></td>
					<td class="text-center"><h4>Status</h4></td>
					<td class="text-center"><h4>Type</h4></td>
				</tr>
			</thead>
			<tbody>
				<?php if(count($joborder_summary) > 0):?>
				<?php foreach($joborder_summary as $joborder_list): ?>
					<tr>
						<td class="text-center"><a href="#"><?php echo $joborder_list->id; ?></a></td>
						<td class="text-center"><?php echo $joborder_list->asset_name; ?></td>
						<td class="text-center"><?php echo $joborder_list->job_trans_created; ?></td>
						<td class="text-center"><?php if($joborder_list->job_trans_status == 0) echo 'Pending'; else echo 'Completed'; ?></td>
						<td class="text-center"><?php if($joborder_list->job_trans_type == 1) echo 'New Transaction'; else echo 'Follow Up'; ?></td>
					</tr>
				<?php endforeach; ?>
				<?php else: ?>
					<tr>
						<td colspan="5">No Transaction yet.</td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>