<!-- <form enctype="multipart/form-data" class="form-horizontal" role="form" method="POST" action="<?php //echo base_url('admin/joborder/edit'); echo @$joborder->id !== NULL ? '/'. @$joborder->id : '' ; ?>"> -->
<form enctype="multipart/form-data" class="form-horizontal" role="form" method="POST" action="<?php echo base_url('admin/joborder/createnew'); ?>">
	<!-- search customer -->
	<div class="form-group">
		<!-- <label class="col-md-2">Customer Name</label> -->
		<div class="col-md-12">
			<div class="input-group">
				<input onkeyup="search_customer(this, 1);" id="theCustomer" type="text" class="form-control thekeyword" value="<?php echo @$customer->cus_name; ?>" placeholder="Please search customer" />
				<input name="tbl_customer_id" type="hidden" id="customer_id" value="<?php echo @$customer->id; ?>">
				<span class="input-group-btn">	
					<button class="btn btn-primary" type="submit">GO</button>
				</span>
			</div>
		</div>
		<div class="col-md-12">
			<div style="display:none;" class="showResult">
				<ul class="list-group">
				</ul>
			</div>
		</div>
	</div>
</form>
<!-- divider and the Details of the trasaction-->
<?php if(count(@$properties) > 0):?>
	<?php foreach($properties as $prop_idx => $prop_val): ?>
<!--Property Transaction -->

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-home"></i> &nbsp;<?php echo $prop_val->asset_name; ?></h2>
                <div class="box-icon">
                    <a class="btn btn-setting btn-round btn-default" href="#"><i class="glyphicon glyphicon-cog"></i></a>
                    <a class="btn btn-minimize btn-round btn-default" href="#"><i class="glyphicon glyphicon-chevron-up"></i></a>
                    <a class="btn btn-close btn-round btn-default" href="#"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div style="background:#F3F3F3;" class="box-content clearfix">
            	<div class="row">
	            	<div class="col-md-3">
						<img src="<?php echo base_url($prop_val->asset_photo); ?>" class="img-responsive" alt="Image" style="border:2px solid #999;" width="100%" />
					</div>
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-12">
								<div class="input-group">
									<input type="text" class="form-control contact_key" value="" placeholder="Search contact cerson..." />
									<input name="customer_contact_id" type="hidden" class="customer_contact_id">
									<span class="input-group-btn">
										<button onclick="search_customer_contact(this,2);" class="btn btn-primary" type="button"><i class="fa fa-search"></i></button>
									</span>
								</div>
								<div style="display:none;" class="showResult">
									<ul class="list-group"></ul>
								</div>
							</div>
						</div>
						<br />
						<div class="row">
							<div class="col-md-12">
								<ul class="nav nav-tabs" id="myTab">
									<li class="active"><a href="#property_window<?php echo $prop_idx; ?>">Windows</a></li>
									<li><a href="#property_sofa<?php echo $prop_idx; ?>">Sofa</a></li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="property_window<?php echo $prop_idx; ?>">
										<div class="row">
											<div class="col-md-12">
												<table class="table table-condensed table-hover windowTable">
													<thead>
														<tr>
															<th>Location</th>
															<th>Room</th>
															<th>Short Description</th>
															<th>Measurement</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody>
														<tr class="tableItems">
															<td>Ground floor</td>
															<td>Living Room</td>
															<td>left side window in the living room</td>
															<td>23 x 40 x 21</td>
															<td>
																<div class="btn-group btn-group-xs pull-left" role="group">
																	<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
																		Selections
																		<span class="caret"></span>
																	</button>
																	<ul class="dropdown-menu" style="min-width:0px;font-size:13px;" role="menu">
																		<li><a onclick="showModal(this,1)" href="javascript:void(0);"><i class="fa fa-indent"></i> Add Curtains</a></li>
																		<li><a onclick="showModal(this,2)" href="javascript:void(0);"><i class="fa fa-files-o"></i> Add Blinds</a></li>
																		<li><a onclick="showModal(this,3)" href="javascript:void(0);"><i class="fa fa-chevron-up"></i> Add Tracks</a></li>
																		<li><a onclick="showModal(this,4)" href="javascript:void(0);"><i class="fa fa-compress"></i> Accessories</a></li>
																	</ul>
																</div>
																<a onclick="save_transaction_item(this);" href="javascript:void(0);" style="font-size:18px;color:green;display:block;" class="pull-right remover">
																	<i class="fa fa-check-circle"></i>
																</a>
																<a onclick="remove($(this).parent().parent().remove());" href="javascript:void(0);" style="font-size:18px;color:red;display:none;margin-right:10px;" class="pull-right remover">
																	<i class="fa fa-times-circle"></i>
																</a>
															</td>
														</tr>
													</tbody>
												</table>
												<button type="button" onclick="addNewItem(this);" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Add New</button>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="property_sofa<?php echo $prop_idx; ?>">
										<div class="row">
											<div class="col-md-12">
												<table class="table table-condensed table-hover windowTable">
													<thead>
														<tr>
															<th>Location</th>
															<th>Room</th>
															<th>Short Description</th>
															<th>Measurement</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody>
														<tr class="tableItems">
															<td>Ground floor</td>
															<td>Living Room</td>
															<td>left side window in the living room</td>
															<td>23 x 40 x 21</td>
															<td>
																<div class="btn-group btn-group-xs pull-left" role="group">
																	<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
																		Selections
																		<span class="caret"></span>
																	</button>
																	<ul class="dropdown-menu" style="min-width:0px;font-size:13px;" role="menu">
																		<li><a onclick="showModal(this,1)" href="javascript:void(0);"><i class="fa fa-indent"></i> Add Curtains</a></li>
																		<li><a onclick="showModal(this,2)" href="javascript:void(0);"><i class="fa fa-files-o"></i> Add Blinds</a></li>
																		<li><a onclick="showModal(this,3)" href="javascript:void(0);"><i class="fa fa-chevron-up"></i> Add Tracks</a></li>
																		<li><a onclick="showModal(this,4)" href="javascript:void(0);"><i class="fa fa-compress"></i> Accessories</a></li>
																	</ul>
																</div>
																<a onclick="remove($(this).parent().parent().remove());" href="javascript:void(0);" style="font-size:15px;color:red;display:none;" class="pull-right remover"><i class="fa fa-times-circle"></i></a>
															</td>
														</tr>
													</tbody>
												</table>
												<button type="button" onclick="addNewItem(this);" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Add New</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div> 	
					</div>
				</div>
            </div>
        </div>
	</div>
</div>
<?php endforeach; ?>
<?php else: ?>
	<div class="row">
		<div class="col-md-12">
			<div class="box-inner">
	            <div class="box-header well">
	                <h2><i class="glyphicon glyphicon-home"></i> &nbsp;<?php echo @$prop_val->asset_name; ?></h2>
	                <div class="box-icon">
	                    <a class="btn btn-setting btn-round btn-default" href="#"><i class="glyphicon glyphicon-cog"></i></a>
	                    <a class="btn btn-minimize btn-round btn-default" href="#"><i class="glyphicon glyphicon-chevron-up"></i></a>
	                    <a class="btn btn-close btn-round btn-default" href="#"><i class="glyphicon glyphicon-remove"></i></a>
	                </div>
	            </div>
	            <div class="box-content clearfix">
					<h3>Select customer first</h3>
				</div>
			</div>
		</div>
	</div>
<?php endif;?>