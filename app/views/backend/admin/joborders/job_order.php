<div class="row">
	<div class="box col-md-12">
		<div class="box-inner">
			<div data-original-title="" class="box-header well">
				<h2><i class="glyphicon glyphicon-book"></i> Job Order Transaction</h2>
			</div>
			<div class="box-content" style="display: block;">
				<div class="row">
					<div class="col-md-6">
						<label class="control-label">Status: <?php if($job_transaction_item[0]->job_trans_status == 0) echo '<strong style="color:red;">PENDING</strong>'; ?></label>
					</div>
					<div class="col-md-6">
						<div class="clearfix">
							<label class="control-label pull-right">Transaction type: <?php if($job_transaction_item[0]->job_trans_type == 1) echo '<strong style="color:red;">FIRST TIME</strong>'; ?></label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<table class="table table-bordered responsive">
							<tbody>
								<tr>
									<td style="background:#F0F0F0;"><strong>Customer</strong></td>
									<td><h4><?php echo $job_transaction_item[0]->cus_name; ?></h4></td>
									<td style="background:#F0F0F0;"><strong>Date Created</strong></td>
									<td><h4><?php echo date("M d Y", strtotime($job_transaction_item[0]->job_trans_created)); ?></h4></td>
								</tr>
								<tr>
									<td style="background:#F0F0F0;"></td>
									<td></td>
									<td style="background:#F0F0F0;"><strong>Last Updated</strong></td>
									<td><h4><?php echo date("M d Y", strtotime($job_transaction_item[0]->job_trans_modify)); ?></h4></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="box col-md-12">
		<div class="box-inner">
			<div data-original-title="" class="box-header well">
				<h2><i class="glyphicon glyphicon-tasks"></i> Transaction Items</h2>
			</div>
			<div class="box-content" style="display: block;">
				<div class="row">
					<div class="col-md-12">
						<table class="table table-striped table-bordered responsive">
							<thead>
								<tr>
									<th class="text-center">Location</th>
									<th class="text-center">Room</th>
									<th class="text-center">Description</th>
									<th class="text-center">Measurement (ft.)</th>
									<th class="text-center">Type</th>
									<th class="text-center">Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($job_transaction_item) > 0):?>
									<?php foreach($job_transaction_item as $job_transaction_item_idx => $job_transaction_item_val):?>
										<tr>
											<td><?php echo $job_transaction_item_val->location; ?></td>
											<td><?php echo $job_transaction_item_val->room; ?></td>
											<td><?php echo $job_transaction_item_val->short_desc; ?></td>
											<td class="text-center"><?php echo $job_transaction_item_val->full_height.' x '.$job_transaction_item_val->window_height.' x '.$job_transaction_item_val->window_width; ?></td>
											<td class="text-center"><?php if($job_transaction_item_val->asset_type == 0) echo 'Curtain'; else echo 'Sofa'; ?></td>
											<td class="text-center"> 
												<div class="btn-group">
											        <button type="button" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-hand-up"></i> Action</button>
											        <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle btn-sm"><span class="caret"></span></button>
											        <ul class="dropdown-menu">
											            <li><a data-toggle="modal" href="#modal_item">Add Accessories</a></li>
											            <li><a data-toggle="modal" href="#modal_item">Add Curtain Type</a></li>
											            <li><a data-toggle="modal" href="#modal_item">Add Blind Type</a></li>
											            <li><a data-toggle="modal" href="#modal_item">Add Track Type</a></li>
											        </ul>
											    </div>
											</td>
										</tr>
									<?php endforeach; ?>
								<?php else: ?>
									<?php echo '<tr><td colspan="6">No item in this transaction...</td></tr>'; ?>
								<?php endif; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- item modal selection -->
<div class="modal fade" id="modal_item">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">ss
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->