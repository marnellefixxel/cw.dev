<!DOCTYPE html>
<html lang="en" base-url = "<?php echo base_url(); ?>" ng-app="cw">
<head>
    <!--
        ===
        This comment should NOT be removed.

        Charisma v2.0.0

        Copyright 2012-2014 Muhammad Usman
        Licensed under the Apache License v2.0
        http://www.apache.org/licenses/LICENSE-2.0

        http://usman.it
        http://twitter.com/halalit_usman
        ===
    -->
    <meta charset="utf-8">
    <title><?php echo $meta_title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="COTTONWARE, Customer Record Management">
    <meta name="author" content="Marnelle">

    <!-- The styles -->
    <?php 
    if(!is_null($styles) && count($styles) != 0) {
        foreach ($styles as $thestyles) {
            echo '<link href="'.base_url($thestyles).'" rel="stylesheet">'."\n";
        }
    }
    ?>
    <!-- jQuery -->
    <script src="<?php echo base_url();?>bower_components/jquery/jquery.min.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>  
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="<?php echo base_url();?>img/favicon.ico">

</head>
<body style="background-color: #DCDCDC;">

	<?php $no_visible_elements = true;?>

    <div class="container-fluid">
    	<div class="row">
    	    <div class="col-md-12 center login-header">
    	        <!-- <h2>Welcome to Charisma</h2> -->
                <h2><img src="<?php echo base_url('img/logo.png'); ?>" class="img-responsive center" alt="Image" width="500"></h2>
    	    </div>
    	    <!--/span-->
    	</div><!--/row-->
    	<div class="row" style="padding: 15px;">
    	    <div class="well col-md-5 center login-box">
    	        <div class="alert alert-info">
    	            Please login with your Username and Password.
    	        </div>

    	        <form method="POST" action="<?php echo base_url('admin/user/login');?>">
    				<small class="error"><?php echo form_error('username'); ?></small>
    				<div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                        <input type="text" name="username" class="form-control" placeholder="Username" value="<?php echo set_value('username'); ?>">
                    </div>
    			    <br />
    			    <small class="error"><?php echo form_error('password'); ?></small>
    			    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                        <input type="password" name="password" class="form-control" placeholder="Password" value="">
                    </div>

                    <div class="input-prepend">
                        <label class="remember" for="remember"><input type="checkbox" id="remember"> Remember me</label>
                    </div>

    			    <p class="center col-md-5">
                        <button type="submit" class="btn btn-primary">Login</button>
                    </p>
    			</form>
    	    </div>
    	    <!--/span-->
    	</div><!--/row-->
    </div>

	<?php 
		if(!is_null($scripts) && count($scripts) != 0) {
			foreach ($scripts as $thescripts) {
			echo '<script type="text/JavaScript" src="'.base_url($thescripts).'"></script>'."\n";
		}
	}

	?>
	</body>
</html>