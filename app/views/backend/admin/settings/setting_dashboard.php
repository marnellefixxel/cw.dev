<div class="row">
	<!-- first part of page -->
	<div class="col-md-3">
		<!-- upload logo in the system -->
		<!-- <div class="panel panel-default">
			<div class="panel-heading panel_header_color">
				<h3 class="panel-title">Upload Logo</h3>
			</div>
			<div class="panel-body">
				<form enctype="multipart/form-data" action="" method="POST" class="form-horizontal" role="form">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">System Logo:</label>
							<div class="fileinput fileinput-new input-group" data-provides="fileinput">
								<div class="form-control file-mask flat-radius" data-trigger="fileinput"><i class="fa fa-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
								<span class="input-group-addon btn btn-default btn-file btn_edge">
								<span class="fileinput-exists">Change</span>
								<span class="fileinput-new">Select file</span>                                    
									<input class="input-sm" type="file" name="img_logo[]">
								</span>
								<a href="#" class="flat-radius input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
							</div>
							<button type="submit" class="btn btn-primary">Upload Logo</button>
						</div>
					</div>
				</form>
			</div>
		</div> -->
		<!-- job category -->
		<div class="panel panel-default">
			  <div class="panel-heading panel_header_color">
					<h3 class="panel-title">Job Order Category</h3>
			  </div>
			  <div class="panel-body">
					<form action="" method="POST" class="form-horizontal" role="form">
						<div class="col-md-12">
							<div style="margin-bottom:5px;" class="form-group">
								<label class="control-label" for="">Name </label>
								<input type="text" class="form-control input-sm" id="category_name" placeholder="Category Name">
								<small id="show_subcat"><a href="javascript:void(0);">Add Sub Category</a></small>
								<div style="margin-top:5px;display:none;" id="add_subCat">
									<input onkeyup=get_subcat(fevent); type="text" class="form-control input-sm" id="subCat_name" placeholder="Sub Category Name" />
								</div>
								<div class="subcat_list">
									<ul class="list-group" style="margin-top:5px;"></ul>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label" for="">Description </label>
								<textarea class="form-control input-sm" rows="2" id="cat_description"placeholder="Category Description"></textarea>
							</div>

							<div class="form-group pull-right">
								<div class="btn-group" data-toggle="buttons">
								  <label class="btn btn-warning btn-xs active">
								    <input type="radio" id="" class="cat_stat" value="1" autocomplete="off"> Enable
								  </label>
								  <label class="btn btn-warning btn-xs">
								    <input type="radio" id="" class="cat_stat" value="0" autocomplete="off"> Disable
								  </label>
								</div>
							</div>

							<div class="form-group">
								<button onclick=ADD_JOB_CATEGORY(); type="button" class="form-control btn btn-primary"><span id="add_jobcat_btn">Add</span></button>
							</div>
						</div>
					</form>
			  </div>
		</div>
	</div>

	<!-- second part of page -->
	<div class="col-md-9">
		<div class="row">
			<!-- first in second part of page -->
			<div class="col-md-8">
				<div class="panel panel-default">
					  <div class="panel-heading panel_header_color">
							<h3 class="panel-title">Job Category</h3>
					  </div>
						<div class="panel-body">
							<form action="" method="POST" class="form-horizontal" role="form">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label" for="">Name </label>
										<input type="text" class="form-control input-sm" id="job_class_name" placeholder="Job Classification Name">
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-6">
												<?php //dump($job_category); ?>
												<label class="control-label" for="">Supplier </label>
												<select name="" id="tbl_supplier_id" class="form-control input-sm">
													<option value="">-- Select Supplier --</option>
													<?php 
														if(count($supplier) > 0):
														foreach ($supplier as $suppliers):
													?>
														<option value="<?php echo $suppliers->id; ?>"><?php echo $suppliers->supp_co_name; ?></option>
													<?php 
														endforeach;
														else:
													?>
														<option value="">No Supplier in the system</option>
													<?php
														endif;
													?>
												</select>
											</div>
											<div class="col-md-6">
												<label class="control-label" for="">Job Category </label>
												<select name="" id="job_category_id" class="form-control input-sm">
													<option value="">-- Select Job Category --</option>
													<?php 
														if(count($job_category) > 0):
														foreach ($job_category as $job_category):
													?>
														<option value="<?php echo $job_category->id; ?>"><?php echo $job_category->job_name; ?></option>
													<?php 
														endforeach;
														else:
													?>
														<option value="">No Job Category in the system</option>
													<?php
														endif;
													?>
												</select>
											</div>
										</div>
									</div>
									<div class="form-group">

										<label class="control-label" for="">Job Type </label><div style="color:red;display:none;" id="disp"></div>
										<select name="" id="sub_category" class="form-control input-sm">
											<option value="">-- Select Job Type --</option>
										</select>
									</div>
									<div class="form-group">
										<label class="control-label" for="">Description </label>
										<textarea class="form-control input-sm" rows="4" id="job_class_desc" placeholder="Category Description"></textarea>
									</div>
									<div class="form-group">
										<button onclick = ADD_JOB_CLASSIFICATION(); type="button" class="btn btn-primary"><span id="add_jobclass_btn">Add</span></button>
									</div>
									
								</div>
							</form>
						</div>
				</div>
			</div>
			<!-- second in second part of page -->
			<div class="col-md-4">
				<!-- Supplier moderation -->
				<div class="panel panel-default">
					  <div class="panel-heading panel_header_color">
							<h3 class="panel-title">Supplier</h3>
					  </div>
					  <div class="panel-body">
							<form action="" method="POST" class="form-horizontal" role="form">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label" for="">Name </label>
										<input type="text" class="form-control input-sm" id="supp_name" placeholder="Supplier Name">
									</div>
									<div class="form-group">
										<label class="control-label" for="">Representative </label>
										<input type="text" class="form-control input-sm" id="supp_contact_name" placeholder="Contact Person">
									</div>
									<div class="form-group">
										<label class="control-label" for="">Contact Number </label>
										<input type="text" class="form-control input-sm" id="supp_contact_num" placeholder="Contact Number">
									</div>
									<div class="form-group">
										<label class="control-label" for="">Email Address </label>
										<input type="email" class="form-control input-sm" id="supp_email" placeholder="Email Address">
									</div>
									
									<div class="form-group">
										<button onclick = ADD_NEW_SUPPLIER(); type="button" class="form-control btn btn-primary"><span id="add_supplier_btn">Add</span></button>
									</div>
								</div>
							</form>
					  </div>
				</div>
			</div>
		</div>
		<hr style="border:1px dashed #999;" />
		<div class="row">
			<div class="col-md-12">
				<div class="custom_tab" role="tabpanel">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#supplier" aria-controls="supplier" role="tab" data-toggle="tab">View All Supplier</a></li>
						<li role="presentation"><a href="#jo_order_cat" aria-controls="jo_order_cat" role="tab" data-toggle="tab">Job Order Category</a></li>
						<li role="presentation"><a href="#job_classification" aria-controls="job_classification" role="tab" data-toggle="tab">Job Classification</a></li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content custom_tab_content">
						<div role="tabpanel" class="tab-pane active" id="supplier">
							<div id="table-container" class="table-responsive">
								<h3>Supplier Listing</h3>
								<table class="table table-bordered table-hover">
									<thead>
										<tr style="background:#ccc;">
											<th class="text-center">ID</th>
											<th class="text-center">Supplier name</th>
											<th>Contact Person</th>
											<th>Contact #</th>
											<th>Contact Email</th>
											<th class="text-center">Edit</th>
											<th class="text-center">Remove</th>
										</tr>
									</thead>
									<tbody id="supplier_table">
										<?php if(count($supp_summary) > 0): ?>
											<?php foreach($supp_summary as $supplier1): ?>
												<tr>
													<td class="text-center"><?php echo $supplier1->id; ?></td>
													<td><?php echo $supplier1->supp_co_name; ?></td>
													<td><?php echo $supplier1->supp_contact_person; ?></td>
													<td><?php echo $supplier1->supp_contact_num; ?></td>
													<td><?php echo $supplier1->supp_email; ?></td>
													<td class="text-center"><a href=""><i class="fa fa-edit"></i></a></td>
													<td class="text-center"><a href=""><i class="fa fa-times"></i></a></td>
												</tr>
											<?php endforeach; ?>
										<?php else: ?>
												<tr>
													<td>No Supplier Found in the System</td>
												</tr>
										<?php endif; ?>
									</tbody>
								</table>
								<?php echo '<div id="ajax-table">'.$pagination.'</div>'; ?>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="jo_order_cat">
							<div id="table-container1" class="table-responsive">
								<h3>Job Category Listing</h3>
								<table class="table table-bordered table-hover">
									<thead>
										<tr style="background:#ccc;">
											<th class="text-center">ID</th>
											<th class="text-center">Category Name</th>
											<th>Description</th>
											<th class="text-center">Edit</th>
											<th class="text-center">Remove</th>
										</tr>
									</thead>
									<tbody>
										<?php if(count($job_cat1) > 0): ?>
											<?php foreach($job_cat1 as $job_cat11): ?>
												<tr>
													<td class="text-center"><?php echo $job_cat11->id; ?></td>
													<td><?php echo $job_cat11->job_name; ?></td>
													<td><?php echo $job_cat11->job_desc; ?></td>
													<td class="text-center"><a href=""><i class="fa fa-edit"></i></a></td>
													<td class="text-center"><a href=""><i class="fa fa-times"></i></a></td>
												</tr>
											<?php endforeach; ?>
										<?php else: ?>
												<tr>
													<td>No Job Category Found in the System </td>
												</tr>
										<?php endif; ?>
									</tbody>
								</table>
								<?php echo '<div id="ajax-table1">'.$pagination1.'</div>'; ?>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="job_classification">
							<div id="table-container2" class="table-responsive">
								<h3>Job Classification Listing</h3>
								<table class="table table-bordered table-hover">
									<thead>
										<tr style="background:#ccc;">
											<th class="text-center">ID</th>
											<th class="text-center">Category</th>
											<th class="text-center">Supplier</th>
											<th class="text-center">Class Name</th>
											<th class="text-center">Formula</th>
											<th>Description</th>
											<th class="text-center">Edit</th>
											<th class="text-center">Remove</th>
										</tr>
									</thead>
									<tbody>
										<?php if(count($job_class) > 0): ?>
											<?php foreach($job_class as $job_class1): ?>
												<tr>
													<td class="text-center"><?php echo $job_class1->job_class_id; ?></td>
													<td><?php echo $job_class1->job_name; ?></td>
													<td><?php echo $job_class1->supp_co_name; ?></td>
													<td><?php echo $job_class1->job_class_name; ?></td>
													<td><?php echo $job_class1->job_formula; ?></td>
													<td><?php echo $job_class1->job_desc; ?></td>
													<td class="text-center"><a href=""><i class="fa fa-edit"></i></a></td>
													<td class="text-center"><a href=""><i class="fa fa-times"></i></a></td>
												</tr>
											<?php endforeach; ?>
										<?php else: ?>
												<tr>
													<td>No Job Category Found in the System </td>
												</tr>
										<?php endif; ?>
									</tbody>
								</table>
								<?php echo '<div id="ajax-table1">'.$pagination2.'</div>'; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<br />