<?php include 'config.php' ?>
<!DOCTYPE html>
<html lang="en" base-url = "<?php echo base_url(); ?>" ng-app="cw">
<head>
    <!--
        ===
        This comment should NOT be removed.

        Charisma v2.0.0

        Copyright 2012-2014 Muhammad Usman
        Licensed under the Apache License v2.0
        http://www.apache.org/licenses/LICENSE-2.0

        http://usman.it
        http://twitter.com/halalit_usman
        ===
    -->
    <meta charset="utf-8">
    <title><?php echo $meta_title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="COTTONWARE, Customer Record Management">
    <meta name="author" content="Marnelle">

    <!-- The styles -->
    <?php 
    if(!is_null($styles) && count($styles) != 0) {
        foreach ($styles as $thestyles) {
            echo '<link href="'.base_url($thestyles).'" rel="stylesheet">'."\n";
        }
    }
    ?>
    <link href='<?php echo base_url();?>bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <!-- jQuery -->
    <script src="<?php echo base_url();?>bower_components/jquery/jquery.min.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>  
    <![endif]-->

    <!-- The fav icon -->
    <link rel="shortcut icon" href="<?php echo base_url();?>img/favicon.ico">

</head>

<body>
<!-- topbar starts -->
<div class="navbar navbar-default" role="navigation">
    <div class="navbar-inner">
        <button type="button" class="navbar-toggle pull-left animated flip">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo base_url('admin/dashboard'); ?>">
            <img alt="Cottonware CRM" style="width:200px;" src="<?php echo base_url();?>img/logo.png" />
        </a>
        <!-- user dropdown starts -->
        <div class="btn-group pull-right">
            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> <?php echo $this->session->userdata('name');?></span>
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#">Profile</a></li>
                <li class="divider"></li>
                <li><a href="<?php echo base_url('admin/user/logout'); ?>">Logout</a></li>
            </ul>
        </div>
        <!-- user dropdown ends -->
    </div>
</div>
<!-- topbar ends -->
<div class="ch-container">
    <div class="row">
        <!-- left menu starts -->
        <div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">

                    </div>
                    <ul class="nav nav-pills nav-stacked main-menu">
                        <li class="nav-header">Main</li>
                        <li>
                            <a class="ajax-link" href="<?php echo base_url('admin/dashboard'); ?>"><i class="glyphicon glyphicon-home"></i><span> Dashboard</span></a>
                        </li>
                        <li>
                            <a class="ajax-link" href="<?php echo base_url('admin/calendar'); ?>"><i class="glyphicon glyphicon-calendar"></i><span> Calendar</span></a>
                        </li>
                        <li>
                            <a class="ajax-link" href="<?php echo base_url('admin/customer'); ?>"><i class="glyphicon glyphicon-user"></i><span> Customers</span></a>
                        </li>
                        
                        <li class="nav-header hidden-md">SETTINGS / MODERATION</li>
                        <li>
                            <a class="ajax-link" href="<?php echo base_url('admin/supplier'); ?>"><i class="glyphicon glyphicon-tasks"></i><span> Settings</span></a>
                        </li>
                        <!-- <li class="accordion">
                            <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Accordion Menu</span></a>
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="#">Child Menu 1</a></li>
                                <li><a href="#">Child Menu 2</a></li>
                            </ul>
                        </li> -->
                    </ul>
                </div>
            </div>
        </div>
        <!-- left menu ends -->
        <!-- start of the content -->
        <div id="content" class="col-lg-10 col-sm-10">


