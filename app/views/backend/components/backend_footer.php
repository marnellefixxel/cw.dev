	        </div><!--end of the content -->
	    </div><!--/ end of row-->
	</div><!--/ch-container-->
	<hr>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal">×</button>
	                <h3>Settings</h3>
	            </div>
	            <div class="modal-body">
	                <p>Here settings can be configured...</p>
	            </div>
	            <div class="modal-footer">
	                <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
	                <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
	            </div>
	        </div>
	    </div>
	</div>
	<div class="ch-container">
		<footer class="row">
		    <p class="col-md-9 col-sm-9 col-xs-12 copyright"> 
		    	<a href="http://usman.it" target="_blank">
		    		www.cottonware.com.sg
		    	</a> &copy; 2012 - <?php echo date('Y') ?></p>

		    <p class="col-md-3 col-sm-3 col-xs-12 powered-by">
		    	Powered & Developed by: 
		    	<a href="http://azazabizsolutions.com" target="_blank">
		    		Azaza Biz Soutions
		    	</a>
		    </p>
		</footer>
	</div>

	<!-- external javascript -->
		<?php 
			if(!is_null($scripts) && count($scripts) != 0) {
				foreach ($scripts as $thescripts) {
				echo '<script type="text/JavaScript" src="'.base_url($thescripts).'"></script>'."\n";
			}
		}

		?>
	</body>
</html>
