<?php
class M_customer extends MY_Model {
	protected $_table_name = 'tbl_customers';
	protected $_order_by   = 'id ASC';
	protected $_timestamps = TRUE;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('customer/m_customer_contact');
	}

	public function describeCustomer($customers)
	{	
		$arr = array();

		if(!is_array($customers)):
			$arr[] = $customers;
		else:	
			$arr   = $customers;
		endif;
		
		for($i=0;$i<count($arr);$i++):
			$arr[$i]->cus_fax_num     = json_decode($arr[$i]->cus_fax_num);
			$arr[$i]->cus_hom_num     = json_decode($arr[$i]->cus_hom_num);
			$arr[$i]->cus_han_num     = json_decode($arr[$i]->cus_han_num);
			$arr[$i]->cus_off_num     = json_decode($arr[$i]->cus_off_num);
			$arr[$i]->cus_email       = json_decode($arr[$i]->cus_email);
			$arr[$i]->contact_persons = $this->m_customer_contact->get_by(array('customer_id'=>$arr[$i]->id,'status'=>1));
		endfor;
		
		return $arr;
	}
	
}	