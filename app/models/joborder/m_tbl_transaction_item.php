<?php
class M_tbl_transaction_item extends MY_Model 
{
    protected $_table_name  = 'tbl_transaction_item';
    protected $_order_by  	= 'tbl_transaction_item.id';

    function __construct() {
        parent::__construct();
    }

    public function get_subitem($item_id, $subcat_type)
    {
		$this->db->select('tbl_job_subcat.id, tbl_job_subcat.job_type_name');    	
		$this->db->from('tbl_transaction_type_selection');
		$this->db->where('tbl_transaction_type_selection.tbl_transaction_item_id',$item_id);
		$this->db->where('tbl_job_subcat.sub_cat_type',$subcat_type);
		$this->db->join('tbl_job_subcat','tbl_job_subcat.id=tbl_transaction_type_selection.curtain_type', 'left');
		return $this->db->get()->result();
    }

    public function property_count($id)
    {
        $this->db->select('*');
        $this->db->from($this->_table_name);
        $this->db->where($this->_table_name.'.tbl_job_transaction_id', $id);
        $this->db->group_by($this->_table_name.'.tbl_asset_id');
        return $this->db->get()->result();
    }

} # end of class


?>