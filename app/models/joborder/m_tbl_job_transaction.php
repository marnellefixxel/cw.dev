<?php
class M_tbl_job_transaction extends MY_Model 
{
    protected $_table_name  = 'tbl_job_transaction';
    protected $_order_by  	= 'tbl_job_transaction.id';

    function __construct() {
        parent::__construct();
    }

    public function dashboardJobSummary()
    {
    	$this->db->select($this->_table_name.'.*,tbl_customers.cus_name, tbl_asset.asset_name');
    	$this->db->from($this->_table_name);
    	$this->db->join('tbl_customers', 'tbl_customers.id=tbl_job_transaction.tbl_customers_id', 'left');
    	$this->db->join('tbl_transaction_item', 'tbl_transaction_item.tbl_job_transaction_id=tbl_job_transaction.id', 'left');
    	$this->db->join('tbl_asset', 'tbl_asset.id=tbl_transaction_item.tbl_asset_id', 'left');
    	return $this->db->get()->result();
    }

} # end of class
?>