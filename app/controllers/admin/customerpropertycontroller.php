<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Customerpropertycontroller extends Admin_Controller {

	public function __construct() 
    {
        parent::__construct();
        $this->load->model('customer/m_customer');
        $this->load->model('customer/m_customer_contact');
        $this->data['active_nav'] = "customer";
    }

    //index function
    public function index()
    {
        $this->data['subview'][]  = "backend/admin/customer/customer-list";
        $this->load->view('backend/admin/home', $this->data, FALSE);
    }  
    
}