<?php

class Calendar extends Admin_Controller 
{
	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$user_id = $this->session->userdata('id');
		$this->data['subview'][] = 'backend/admin/calendar';
		$this->load->view('backend/admin/home',$this->data);
	}
}

?>