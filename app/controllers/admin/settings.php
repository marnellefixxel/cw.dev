<?php

class Settings extends Admin_Controller 
{
	public function __construct() {
		parent::__construct();

		$this->load->model('supplier/m_tbl_supplier');
		$this->load->model('settings/m_tbl_job_category');
		$this->load->model('settings/m_tbl_job_subcat');
		$this->load->model('settings/m_tbl_job_classification');

		$user_id = $this->session->userdata('id');
	}

	public function index()
	{
		$this->load->library('pagination');

		// load all the supplier in the list
		$table_name 				 = 'tbl_supplier';
		$total						 = count($this->db->get($table_name)->result());
		$per_pg						 = 6;
		$offset						 = $this->uri->segment(3);
		$data['base']				 = $this->config->item('base_url');
		$this->data['pagination'] 	 = $this->__ajax_paginate($data['base'].'admin/settings', $total, $per_pg);
		$this->data['supp_summary']  = $this->db->get($table_name,$per_pg,$offset)->result();

		// load all the job category in the list
		$table_name1 				 = 'tbl_job_category';
		$total1						 = count($this->db->get($table_name1)->result());
		$per_pg1					 = 6;
		$offset1					 = $this->uri->segment(3);
		$data['base1']				 = $this->config->item('base_url');
		$this->data['pagination1']   = $this->__ajax_paginate($data['base1'].'admin/settings', $total1, $per_pg1);
		$this->data['job_cat1'] 	 = $this->db->get($table_name1,$per_pg1,$offset1)->result();

		// load all the job classification in the list
		$table_name2 				 = 'tbl_job_classification';
		$total2						 = count($this->db->get($table_name2)->result());
		$per_pg2					 = 6;
		$offset2					 = $this->uri->segment(3);
		$data['base2']				 = $this->config->item('base_url');
		$this->data['pagination2']   = $this->__ajax_paginate($data['base1'].'admin/settings', $total2, $per_pg2);
		$this->data['job_class'] 	 = $this->m_tbl_job_classification->get_classification($table_name2,$per_pg2,$offset2);

		// ==============
		$this->data['supplier'] 	 = $this->m_tbl_supplier->get();
		$this->data['job_category']  = $this->m_tbl_job_category->get();

		$this->data['subview'][] 	 = 'backend/admin/single';
		$this->load->view('backend/admin/home',$this->data);
	}

	# add customer via ajax
	public function ADD_SUPPLIER()
	{
		$supp_co_name 			= $this->input->post('supp_name');
		$supp_contact_person 	= $this->input->post('supp_contact_name');
		$supp_contact_num 		= $this->input->post('supp_contact_num');
		$supp_email 			= $this->input->post('supp_email');

		if(empty($supp_co_name))
		{
			echo json_encode(array('stat'=>false, 'msg'=>'Supplier name must not be empty'));
		}
		else
		{
			$data = array(
				'supp_co_name' 			=> $supp_co_name,
				'supp_contact_person' 	=> $supp_contact_person,
				'supp_contact_num' 		=> $supp_contact_num,
				'supp_email' 			=> $supp_email
			);

			$save_supplier = $this->m_tbl_supplier->save($data);
			$thedata = $this->m_tbl_supplier->get($save_supplier);

			if(count($save_supplier) > 0)
			{
				echo json_encode(array('stat'=>true, 'msg'=>'New supplier successfully added', 'content'=>$thedata));
			}
		}
	}

	# add job category via ajax
	public function ADD_JOB_ORDER()
	{
		$job_name 		= $this->input->post('job_name');
		$job_desc 		= $this->input->post('job_desc');
		$job_cat_stat 	= $this->input->post('cat_stat');
		$subCategory	= $this->input->post('subCategory');

		if(empty($job_name))
		{
			echo json_encode(array('stat'=>false, 'msg'=>'Category name must not be empty'));
		}
		else
		{
			$data = array(
				'job_name' 		=> $job_name,
				'job_desc' 		=> $job_desc,
				'is_available' 	=> $job_cat_stat
			);

			$save_cat = $this->m_tbl_job_category->save($data);
			if(count($save_cat) > 0)
			{
				if(!empty($subCategory))
				{
					foreach ($subCategory as $subCategory)
					{
						if(!empty($subCategory))
						{
							$subcat = array(
								'job_type_name' 	=> $subCategory,
								'tbl_job_category_id'	=> $save_cat
							);
							$save_subcat = $this->m_tbl_job_subcat->save($subcat);
						}
					}
				}
			}
			echo json_encode(array('stat'=>true, 'msg'=>'New Job category successfully added'));
		}
	}

	# get sub category via ajax
	public function GET_SUBCATEGORY()
	{
		$parent_jobcat_id = $this->input->post('jobcat_id');

		if(!empty($parent_jobcat_id))
		{
			$subcat_list = $this->m_tbl_job_subcat->get_by(array('tbl_job_category_id'=>$parent_jobcat_id));
			if(count($subcat_list) > 0)
			{
				echo json_encode(array('subcat_list'=>$subcat_list));
			}
		}
	}

	# add new job classification
	public function ADD_JOB_CLASSIFICATION()
	{
		$job_class_name  = $this->input->post('job_class_name');
		$tbl_supplier_id = $this->input->post('tbl_supplier_id');
		$job_category_id = $this->input->post('job_category_id');
		$sub_category    = $this->input->post('sub_category');
		$job_class_desc  = $this->input->post('job_class_desc');

		$data = array(
			'job_class_name' 		=> $job_class_name,
			'job_formula'	 		=> 0,
			'job_desc'	 			=> $job_class_desc,
			'tbl_job_category_id'	=> $job_category_id,
			'tbl_supplier_id'	 	=> $tbl_supplier_id
		);

		$save_job_class = $this->m_tbl_job_classification->save($data);
		if(count($save_job_class) > 0)
		{
			echo json_encode(array('msg'=>'Successfully adding Job Classification'));
		}

	}

} # end of class

?>