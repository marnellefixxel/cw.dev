<?php

class Supplier extends Admin_Controller 
{
	public function __construct() {
		parent::__construct();

		$this->load->model('supplier/m_tbl_supplier');
		$user_id = $this->session->userdata('id');
	}

	public function index() {

		$user_id = $this->session->userdata('id');

		$this->data['page_title'] = 'This is supplier area';
		$this->data['subview'][] = 'backend/admin/settings/setting_dashboard';
		$this->load->view('backend/admin/home',$this->data);
	}

	# add new supplier / edit existing supplier
	public function edit($id = NULL) {
		
		$this->data['supplier'] = $this->m_supplier->get($id);

		$supplier_first_name       = $this->input->post('supplier_first_name');
		$supplier_last_name        = $this->input->post('supplier_last_name');
		$supplier_contact_number   = $this->input->post('supplier_contact_number');
		$supplier_email            = $this->input->post('supplier_email');
		$supplier_address          = $this->input->post('supplier_address');
		$supplier_status           = $this->input->post('supplier_status');
		$now                       = date('Y-m-d H:i:s');

		$data = array(
			'supplier_first_name'     => $supplier_first_name,
			'supplier_last_name'      => $supplier_last_name,
			'supplier_contact_number' => $supplier_contact_number,
			'supplier_email'          => $supplier_email,
			'supplier_address'        => $supplier_address,
			'supplier_status'         => $supplier_status,
			'supplier_create_date'    => $now,
			'supplier_last_update'    => $now
		);

		if($id == NULL)
		{
			if($_POST)
			{
				$result	= $this->m_supplier->save($data);
				if(count($result) > 0)
				{
					$this->session->set_flashdata(array('msg' => 'Saved changes successfully', 'flag' => 'success'));
					redirect(base_url().'admin/supplier/edit/'.$id);
				}
			}

			$this->data['page_title'] = 'Add New Supplier';
			$this->data['subview'][] = 'backend/admin/user/supplier/supplier_form';
		}
		else
		{
			if($_POST)
			{
				$result	= $this->m_supplier->save($data, $id);
				if(count($result) > 0)
				{
					$this->session->set_flashdata(array('msg' => 'Update supplier successfully', 'flag' => 'success'));
					redirect(base_url().'admin/supplier/edit/'.$id);
				}
			}

			$this->data['page_title'] = 'Update Supplier';
			$this->data['subview'][] = 'backend/admin/user/supplier/supplier_form';
		}

		$this->load->view('backend/admin/home',$this->data);
	}

}

?>