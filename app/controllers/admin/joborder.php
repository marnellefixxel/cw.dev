<?php

class Joborder extends Admin_Controller 
{
	public function __construct() 
	{
		parent::__construct();

		$this->load->model('customer/m_customer');
		$this->load->model('customer/m_customer_contact');
		$this->load->model('joborder/m_tbl_asset');
		$this->load->model('joborder/m_tbl_asset_dtl');
		$this->load->model('joborder/m_tbl_asset_sub_dtl');
		$this->load->model('joborder/m_tbl_job_transaction');
		$this->load->model('joborder/m_tbl_transaction_item');
		$this->load->model('joborder/m_tbl_transaction_type_selection');
		$this->load->model('settings/m_tbl_job_subcat');
		$this->load->model('settings/m_tbl_job_classification');
	}

	public function fetching_asset_dtl()
	{
		$property_id = $this->input->post('property_id');
		$result		 = $this->m_tbl_asset_dtl->get_by(array('tbl_asset_id'=>$property_id));

		if(count($result) > 0) {
			echo json_encode(array('success'=>true, 'data'=>$result));
		}
		else echo json_encode(array('success'=>false));
	}

	public function createnew() 
	{
		$user_id 		= $this->session->userdata('id');
		$contact_id 	= $this->input->post('contact_id');
		$property_id 	= $this->input->post('property_id');
		$customer_id 	= $this->input->post('customer_id');
		$item 			= json_decode($this->input->post('item'));
		$retval			= FALSE;

		// handling the creation of transaction
		$jo_data = array(
			'tbl_users_id'		=> $user_id,
			'tbl_customers_id'	=> $customer_id,
			'job_trans_created'	=> date("Y-m-d H:i:s")
		);
		/*======================================================================================================
		// once the transaction is save, it will also save all the transaction item connected to the transaction id
		======================================================================================================*/
		$jo_transaction = $this->m_tbl_job_transaction->save($jo_data);
		if(count($jo_transaction) > 0)
		{
			// handling curtain data
			if(count($item->curtain) > 0):
				// loop and get each item posted
				foreach($item->curtain as $item_idx =>$item_val):
					// check the item_iD if exists in the db
					$check_exist = $this->m_tbl_asset_dtl->get($item_val->assetDTLid);
					
					$item_val->fh 			= str_replace('<br>', '', $item_val->fh);
					$item_val->wh 			= str_replace('<br>', '', $item_val->wh);
					$item_val->ww 			= str_replace('<br>', '', $item_val->ww);
					$item_val->location 	= str_replace('<br>', '', $item_val->location);
					$item_val->room 		= str_replace('<br>', '', $item_val->room);
					$item_val->short_desc 	= str_replace('<br>', '', $item_val->short_desc);

					// if true, create a set varible array and store the item details
					$subjob_data = array(
						'tbl_asset_id'	=> $property_id,
						'location'		=> $item_val->location,
						'room'			=> $item_val->room,
						'short_desc'	=> $item_val->short_desc,
						'full_height'	=> $item_val->fh,
						'window_height'	=> $item_val->wh,
						'window_width'	=> $item_val->ww,
						'asset_type'	=> 0
					);

					// check if the item id is exist, if TRUE, it will update
					if(@$check_exist->id > 0):
						$asset_dtl = $this->m_tbl_asset_dtl->save($subjob_data, $check_exist->id);
					else :
					// if not exist, it wil create new one..
						$asset_dtl = $this->m_tbl_asset_dtl->save($subjob_data);
					endif;

					if(count($asset_dtl) > 0):
						// getting the main job trasaction data to store in the database
						$transaction_item = array(
							'tbl_job_transaction_id' 	=> $jo_transaction,
							'tbl_customer_contact_id' 	=> $contact_id,
							'asset_id' 					=> $property_id,
							'asset_dtl_id' 				=> $asset_dtl
						);
						// store data to table
						$x = $this->m_tbl_transaction_item->save($transaction_item);

						// once the data is stored, it will send a return value of SUCCESS == TRUE in AJAX
						if(count($x) > 0) $retval = TRUE;
						// otherwise, send FALSE
						else $retval = FALSE;
					endif;
				endforeach;
			endif;

			// handling sofa data
			if(count($item->sofa) > 0):
				// loop and get each item posted
				foreach($item->sofa as $item_idx =>$item_val):
					// check the item_iD if exists in the db
					$check_exist = $this->m_tbl_asset_dtl->get($item_val->assetDTLid);
					
					$item_val->fh 			= str_replace('<br>', '', $item_val->fh);
					$item_val->wh 			= str_replace('<br>', '', $item_val->wh);
					$item_val->ww 			= str_replace('<br>', '', $item_val->ww);
					$item_val->location 	= str_replace('<br>', '', $item_val->location);
					$item_val->room 		= str_replace('<br>', '', $item_val->room);
					$item_val->short_desc 	= str_replace('<br>', '', $item_val->short_desc);

					// if true, create a set varible array and store the item details
					$subjob_data = array(
						'tbl_asset_id'	=> $property_id,
						'location'		=> $item_val->location,
						'room'			=> $item_val->room,
						'short_desc'	=> $item_val->short_desc,
						'full_height'	=> $item_val->fh,
						'window_height'	=> $item_val->wh,
						'window_width'	=> $item_val->ww,
						'asset_type'	=> 1
					);

					// check if the item id is exist, if TRUE, it will update
					if(@$check_exist->id > 0):
						$asset_dtl = $this->m_tbl_asset_dtl->save($subjob_data, $check_exist->id);
					else :
					// if not exist, it wil create new one..
						$asset_dtl = $this->m_tbl_asset_dtl->save($subjob_data);
					endif;

					if(count($asset_dtl) > 0):
						// getting the main job trasaction data to store in the database
						$transaction_item = array(
							'tbl_job_transaction_id' 	=> $jo_transaction,
							'tbl_customer_contact_id' 	=> $contact_id,
							'asset_id' 					=> $property_id,
							'asset_dtl_id' 				=> $asset_dtl
						);
						// store data to table
						$x = $this->m_tbl_transaction_item->save($transaction_item);

						// once the data is stored, it will send a return value of SUCCESS == TRUE in AJAX
						if(count($x) > 0) $retval = TRUE;
						// otherwise, send FALSE
						else $retval = FALSE;
					endif;
				endforeach;
			endif;

			if($retval) echo json_encode(array('success'=> true, 'redirect'=>'joborder/edit/', 'trans_id'=>$jo_transaction));
			else echo json_encode(array('success'=> false));
		}	// end of curtain job transaction
	}

	// create and save joborder transaction
	// this part is intentionally separated for debugging purposes
	// since this part is very huge in processing transaction
	public function edit($id=NULL)
	{
		$user_id = $this->session->userdata('id');
		// $this->data['job_transaction'] 		= $this->m_tbl_job_transaction->get($id);
		$select_join = 'tbl_transaction_item.*, 
						tbl_job_transaction.*, 
						tbl_asset_dtl.*, 
						tbl_customers.cus_name, 
						tbl_customers_contact.name,
						tbl_asset.asset_name,
						tbl_asset.asset_photo';

		$joining_array = array();
		$joining_array[0] = new stdClass();
		$joining_array[1] = new stdClass();
		$joining_array[2] = new stdClass();
		$joining_array[3] = new stdClass();
		$joining_array[4] = new stdClass();

		$joining_array[0]->table 		= 'tbl_transaction_item';
		$joining_array[0]->condition 	= 'tbl_job_transaction.id = tbl_transaction_item.tbl_job_transaction_id';
		$joining_array[0]->method 		= 'left';

		$joining_array[1]->table 		= 'tbl_asset_dtl';
		$joining_array[1]->condition 	= 'tbl_asset_dtl.id = tbl_transaction_item.asset_dtl_id';
		$joining_array[1]->method 		= 'left';

		$joining_array[2]->table 		= 'tbl_customers';
		$joining_array[2]->condition 	= 'tbl_customers.id = tbl_job_transaction.tbl_customers_id';
		$joining_array[2]->method 		= 'left';

		$joining_array[3]->table 		= 'tbl_customers_contact';
		$joining_array[3]->condition 	= 'tbl_customers_contact.id = tbl_transaction_item.tbl_customer_contact_id';
		$joining_array[3]->method 		= 'left';

		$joining_array[4]->table 		= 'tbl_asset';
		$joining_array[4]->condition 	= 'tbl_asset.id = tbl_asset_dtl.tbl_asset_id';
		$joining_array[4]->method 		= 'left';

		$this->data['job_transaction_item'] = $this->m_tbl_job_transaction->get_by(array('tbl_job_transaction.id'=>$id),FALSE,$joining_array,$select_join);

		$this->data['subview'][] = 'backend/admin/joborders/job_order';
		$this->load->view('backend/admin/home',$this->data);
	}

	// update the details of the window/sofa
	public function save_update_assetdtl($selector)
	{
		if($selector == 1)
		{
			$data = $this->input->post('asset_data');
			$final_data = array(
				'tbl_asset_id'	=> $data[0]['propertyID'],
				'location'		=> $data[0]['location'],
				'room'			=> $data[0]['room'],
				'short_desc'	=> $data[0]['short_desc'],
				'full_height'	=> $data[0]['fh'],
				'window_height'	=> $data[0]['wh'],
				'window_width'	=> $data[0]['ww']
			);
			$result = $this->m_tbl_asset_dtl->save($final_data, $data[0]['assetDTLid']);
		}
		else if($selector ==2)
		{
			$newWindow = $this->input->post('newWindow');
			$newWindow_data = array(
				'tbl_asset_id'	=> $newWindow[0]['propID'],
				'location'		=> $newWindow[0]['location'],
				'room'			=> $newWindow[0]['room'],
				'short_desc'	=> $newWindow[0]['short_desc'],
				'full_height'	=> $newWindow[0]['fh'],
				'window_height'	=> $newWindow[0]['wh'],
				'window_width'	=> $newWindow[0]['ww'],
				'asset_type'	=> $newWindow[0]['asset_type']
			);
			$result = $this->m_tbl_asset_dtl->save($newWindow_data);
		}

		if(count($result) > 0) {
			$data = $this->m_tbl_asset_dtl->get($result);
			echo json_encode(array('success'=>true, 'data'=>$data));
		}
		else {
			echo json_encode(array('success'=>false));
		}
	}

	// load customer's asset
	public function load_customer_assets()
	{
		$id 		= $this->input->post('id');
		$selector	= $this->input->post('selector');
		$result		= array();

		# selecting the customer asset
		if($selector == 1) {
			$result = $this->m_tbl_asset->get_by(array('tbl_customer_id'=>$id));
		}
		# selecting the property asset level
		else if($selector == 2) {
			$select_join = 'tbl_asset_dtl.asset_dtl_level, tbl_asset_sub_dtl.asset_sub_dtl_type, tbl_asset_sub_dtl.id AS window_id';
			$joining_array = array();
			$joining_array[0] = new stdClass();
			$joining_array[0]->table 		= 'tbl_asset_sub_dtl';
			$joining_array[0]->condition 	= 'tbl_asset_sub_dtl.tbl_asset_dtl_id=tbl_asset_dtl.id';
			$joining_array[0]->method 		= 'left';
			$result = $this->m_tbl_asset_dtl->get_by(array('tbl_asset_dtl.tbl_asset_id'=>$id), FALSE, $joining_array, $select_join);
		}
		
		if(count($result) > 0) {
			echo json_encode(array('success'=>true, 'data'=>$result));
		}
		else
			echo json_encode(array('success'=>false));
	}

	// ajax searching module
	public function search_module()
	{
		$keyword 	= $this->input->post('keyword');
		$keyMatch 	= $this->input->post('keyMatch');
		$selector 	= $this->input->post('selector');
		$where 		= $this->input->post('where');
		$where_col	= $this->input->post('whereCol');

		#search for customer
		if($selector == '1') {
			if(!empty($where)) {
				$return = $this->m_customer->get_by(array($where_col=>$where, '('.$keyMatch.' like "%'.$keyword.'%")'=>NULL));
			}
			else {
				$return = $this->m_customer->get_by(array('('.$keyMatch.' like "%'.$keyword.'%")'=>NULL));
			}
			
			$return = $this->m_customer->describeCustomer($return);

			if(count($return) > 0) {
				echo json_encode(array('success'=>true, 'data'=>$return));
			}
			else {
				echo json_encode(array('success'=>false));
			}
		}

		#search for customer
		if($selector == '2') {
			if(!empty($where)) {
				$return = $this->m_customer_contact->get_by(array($where_col=>$where, '('.$keyMatch.' like "%'.$keyword.'%")'=>NULL));
			}
			else {
				$return = $this->m_customer_contact->get_by(array('('.$keyMatch.' like "%'.$keyword.'%")'=>NULL));
			}
			
			if(count($return) > 0) {
				echo json_encode(array('success'=>true, 'data'=>$return));
			}
			else {
				echo json_encode(array('success'=>false));
			}
		}
	}

	// load supplier given the job sub-category
	public function get_supplier()
	{
		$sub_category = $this->input->post('sub_cat_id');

		// dump($sub_category);

		if(count($sub_category) > 0):
			foreach ($sub_category as $cat_idx => $cat_val):
				$joining_array = array();
				$joining_array[0] = new stdClass();
				$joining_array[0]->table 		= 'tbl_job_subcat';
				$joining_array[0]->condition 	= 'tbl_job_subcat.id=tbl_job_classification.tbl_job_subcat_id';
				$joining_array[0]->method 		= 'left';

				$joining_array[1] = new stdClass();
				$joining_array[1]->table 		= 'tbl_supplier';
				$joining_array[1]->condition 	= 'tbl_supplier.id=tbl_job_classification.tbl_supplier_id';
				$joining_array[1]->method 		= 'left';

				$result->supplier[$cat_idx] = $this->m_tbl_job_subcat->get($cat_val);
				$result = $this->m_tbl_job_classification->get_by(array('tbl_job_subcat_id'=>$cat_val), 
					FALSE, $joining_array,'tbl_supplier.supp_co_name, tbl_job_classification.cost_amount');

				dump($result);
			endforeach;
		endif;
		die;
	}


} #end of class

?>