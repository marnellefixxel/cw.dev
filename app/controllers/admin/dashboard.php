<?php

class Dashboard extends Admin_Controller 
{
	public function __construct() {
		parent::__construct();

		$this->load->model('joborder/m_tbl_job_transaction');
	}

	public function index()
	{
		$user_id = $this->session->userdata('id');
		$this->data['subview'][] = 'backend/admin/user/dashboard';
		$this->load->view('backend/admin/home',$this->data);
	}
}

?>
