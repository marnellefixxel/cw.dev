<?php
class User extends Admin_Controller 
{
	public function __construct() {
		parent::__construct();
		$this->load->model('user/m_tbl_users');
	}

	public function index() 
	{
		$the_user = $this->session->all_userdata();

		$this->data['ses_info']		= $the_user;
		$this->data['page_title'] 	= 'User Area';
		$this->data['subview'] 		= 'backend/admin/single';
		$this->load->view('backend/admin/home',$this->data);
	}

	public function login() {

		$dashboard = 'admin/dashboard';

		$this->m_tbl_users->loggedin() == FALSE || redirect($dashboard);

		$rules = $this->m_tbl_users->rules;
		$this->form_validation->set_rules($rules);

		#validate the input and process
		if($this->form_validation->run() == TRUE) {

			# if the credential is true then procced
			if($this->m_tbl_users->login() == TRUE) {
				redirect($dashboard);
			}
			else {
				$this->session->set_flashdata('error', 'Credential not match.');
				redirect('admin/user/login', 'refresh');
			}
		}

		// $this->data['subview'] = 'backend/admin/user/login';
		$this->load->view('backend/admin/login',$this->data);
	}

	public function logout() 
	{
		$this->m_tbl_users->logout();
		redirect('admin/user/login');
	}

} #end of class

?>