-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 30, 2015 at 07:15 AM
-- Server version: 5.5.36
-- PHP Version: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cw_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) CHARACTER SET latin1 NOT NULL,
  `ip_address` varchar(45) CHARACTER SET latin1 NOT NULL,
  `user_agent` varchar(120) CHARACTER SET latin1 NOT NULL,
  `last_activity` int(10) NOT NULL,
  `user_data` text CHARACTER SET latin1 NOT NULL,
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('67d8ba8634bbe4b8af19ce5b2fe065f0', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0', 1430370708, 'a:5:{s:9:"user_data";s:0:"";s:2:"id";s:1:"1";s:4:"name";s:13:"Marnelle Apat";s:8:"username";s:5:"admin";s:8:"loggedin";b:1;}');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_asset`
--

CREATE TABLE IF NOT EXISTS `tbl_asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tbl_customer_id` int(11) NOT NULL,
  `asset_name` varchar(255) DEFAULT NULL,
  `asset_add_unit` varchar(255) DEFAULT NULL,
  `asset_add_street` varchar(255) DEFAULT NULL,
  `asset_add_bldg` varchar(255) DEFAULT NULL,
  `asset_add_postal` varchar(255) DEFAULT NULL,
  `tbl_contact_person` int(11) NOT NULL,
  `asset_photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`tbl_customer_id`),
  KEY `fk_tbl_asset_tbl_customers1_idx` (`tbl_customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_asset`
--

INSERT INTO `tbl_asset` (`id`, `tbl_customer_id`, `asset_name`, `asset_add_unit`, `asset_add_street`, `asset_add_bldg`, `asset_add_postal`, `tbl_contact_person`, `asset_photo`) VALUES
(1, 1, 'condominium in Las Vegas', '40th floor, 568 unit', '2nd block, square st.', 'Pennsylvannia', '1000', 0, 'img/properties/building.jpg'),
(2, 1, 'house in Salt Lake Utah', 'block 22', 'jumpstreet', 'salt lake city, utah', '2100', 0, 'img/properties/condominium.jpg'),
(3, 1, 'vacation house in california', 'block 24', 'beverly hills st.', 'Beverly, California', '2098', 0, 'img/properties/house.jpg'),
(4, 1, 'rooftop condo in hawaii', 'rooftop hills', 'kawai, street', 'kawai, hawai', '3098', 0, 'img/properties/rooftopcondo.jpg'),
(5, 1, 'my office building', '17th floor', 'maxilom ave.', 'ongtiak bldg., cebu city, PHL', '6000', 0, 'img/properties/vacationhouse.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_asset_dtl`
--

CREATE TABLE IF NOT EXISTS `tbl_asset_dtl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tbl_asset_id` int(11) NOT NULL,
  `location` varchar(255) DEFAULT NULL,
  `room` varchar(255) DEFAULT NULL,
  `short_desc` varchar(45) DEFAULT NULL,
  `full_height` double DEFAULT '0',
  `window_height` double DEFAULT '0',
  `window_width` double DEFAULT '0',
  `asset_type` tinyint(1) DEFAULT '0' COMMENT '0= curtain; 1= sofa;',
  PRIMARY KEY (`id`,`tbl_asset_id`),
  KEY `fk_tbl_property_dtl_tbl_asset1_idx` (`tbl_asset_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_asset_dtl`
--

INSERT INTO `tbl_asset_dtl` (`id`, `tbl_asset_id`, `location`, `room`, `short_desc`, `full_height`, `window_height`, `window_width`, `asset_type`) VALUES
(1, 1, 'Ground Floor', 'bed room', 'first window in the bedroom', 99, 99, 99, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customers`
--

CREATE TABLE IF NOT EXISTS `tbl_customers` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `cus_salutations` text,
  `cus_role` text,
  `cus_name` text,
  `cus_fax_num` text,
  `cus_hom_num` text,
  `cus_han_num` text,
  `cus_off_num` text,
  `cus_email` text,
  `cus_address_blk` text,
  `cus_address_street` text,
  `cus_address_unit` text,
  `cus_address_bldg` text,
  `cus_address_postal` text,
  `cus_remark` text,
  `cus_status` int(2) DEFAULT NULL,
  `datetime_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `datetime_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tbl_customers`
--

INSERT INTO `tbl_customers` (`id`, `cus_salutations`, `cus_role`, `cus_name`, `cus_fax_num`, `cus_hom_num`, `cus_han_num`, `cus_off_num`, `cus_email`, `cus_address_blk`, `cus_address_street`, `cus_address_unit`, `cus_address_bldg`, `cus_address_postal`, `cus_remark`, `cus_status`, `datetime_created`, `datetime_modified`) VALUES
(1, 'Mr.', 'Owner', 'Lester Ag Lastimosa Padul', '["21312312"]', '["3213354"]', '["09169149722"]', '["123213213"]', '["lester@azazabizsolutions.com"]', '5', 'Camella Miramonte', '9', '', '247964', 'Half-giant jinxes peg-leg gillywater broken glasses large black dog Great Hall. Nearly-Headless Nick now string them together, and answer me this, which creature would you be unwilling to kiss? Poltergeist sticking charm, troll umbrella stand flying cars golden locket Lily Potter. Pumpkin juice Trevor wave your wand out glass orbs, a Grim knitted hats. Stan Shunpike doe patronus, suck his soul Muggle-Born large order of drills the trace. Bred in captivity fell through the veil, quaffle blue flame ickle diddykins Aragog. Yer a wizard, Harry Doxycide the woes of Mrs. Weasley Goblet of Fire.', 1, '2015-03-05 05:10:41', '2015-04-05 21:05:43'),
(2, 'Ms.', 'Owner', 'Chynna Delacour', '["123123"]', '["123123"]', '["213123"]', '["123213"]', '["chynna@gmail.com"]', '5', 'Lot 9 Camella-Miramonte', '3', '["chynna@gmail.com"]', '6000', 'a remark', 1, '2015-03-05 05:21:45', '2015-03-06 01:02:22'),
(6, 'Mr.', 'Owner', 'Barack Obama', '["123123","123213","123123123","123213"]', '["12312223123","1233123","123123"]', '["1232123","123213123"]', '["123123","123123123","123123123"]', '["13123213123@asd.com","12312312@asd.com"]', '123', '123123', '123', '123', '123', '123123', 1, '2015-03-05 06:03:40', '2015-03-06 09:10:31'),
(7, 'Mr.', 'Owner', 'Marnelle Apat', '["23123"]', '["123123"]', '["123123123"]', '["123123"]', '["marnelle@azazabizsolutions.com"]', '1', 'Camella-Miramonte', '1', '', '6000', 'Half-giant jinxes peg-leg gillywater broken glasses large black dog Great Hall. Nearly-Headless Nick now string them together, and answer me this, which creature would you be unwilling to kiss? Poltergeist sticking charm, troll umbrella stand flying cars golden locket Lily Potter. Pumpkin juice Trevor wave your wand out glass orbs, a Grim knitted hats. Stan Shunpike doe patronus, suck his soul Muggle-Born large order of drills the trace. Bred in captivity fell through the veil, quaffle blue flame ickle diddykins Aragog. Yer a wizard, Harry Doxycide the woes of Mrs. Weasley Goblet of Fire.', 1, '2015-03-05 06:09:27', '2015-03-09 02:27:18'),
(10, 'Mr.', 'Owner', 'Richard Brown', '["123123"]', '["123123"]', '["213123"]', '["123123"]', '["richardbrown@gmail.com"]', '1', 'Street', '2123123', '12123', '65002', 'a remark', 1, '2015-03-06 02:51:59', '2015-03-06 02:51:59');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customers_contact`
--

CREATE TABLE IF NOT EXISTS `tbl_customers_contact` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `customer_id` int(100) NOT NULL,
  `name` text,
  `contacts` text,
  `email` text,
  `role` text,
  `status` varchar(20) DEFAULT NULL,
  `datetime_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `datetime_modified` text,
  PRIMARY KEY (`id`,`customer_id`),
  KEY `fk_tbl_customers_contact_tbl_customers1_idx` (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_customers_contact`
--

INSERT INTO `tbl_customers_contact` (`id`, `customer_id`, `name`, `contacts`, `email`, `role`, `status`, `datetime_created`, `datetime_modified`) VALUES
(1, 1, 'Jason Cross', '123456468', 'jc@email.com', 'agent', '1', '2015-04-06 06:09:11', '2015-04-06 08:09:53'),
(2, 1, 'Daniel Padilla', '948577493', 'dp@email.com', 'Brother-in-law', '1', '2015-04-06 06:09:49', '2015-04-06 08:09:53');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job_category`
--

CREATE TABLE IF NOT EXISTS `tbl_job_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_name` varchar(255) DEFAULT NULL,
  `job_desc` text,
  `is_available` tinyint(1) DEFAULT '1' COMMENT '0 = not active; 1 = active job;',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_job_category`
--

INSERT INTO `tbl_job_category` (`id`, `job_name`, `job_desc`, `is_available`) VALUES
(1, 'Curtain Fabrication', 'This is a curtain fabrication job category', 1),
(2, 'Sofa Re-upholstery', 'this is the reupholstery job category', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job_classification`
--

CREATE TABLE IF NOT EXISTS `tbl_job_classification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cost_amount` double DEFAULT '0',
  `tbl_job_category_id` int(11) NOT NULL,
  `tbl_supplier_id` int(11) NOT NULL,
  `tbl_job_subcat_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`tbl_job_category_id`,`tbl_supplier_id`),
  KEY `fk_tbl_job_classification_tbl_job_category1_idx` (`tbl_job_category_id`),
  KEY `fk_tbl_job_classification_tbl_supplier1_idx` (`tbl_supplier_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_job_classification`
--

INSERT INTO `tbl_job_classification` (`id`, `cost_amount`, `tbl_job_category_id`, `tbl_supplier_id`, `tbl_job_subcat_id`) VALUES
(1, 10, 1, 1, 1),
(2, 20, 1, 2, 1),
(3, 25, 1, 3, 1),
(4, 15, 1, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job_subcat`
--

CREATE TABLE IF NOT EXISTS `tbl_job_subcat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_type_name` varchar(255) DEFAULT NULL,
  `sub_cat_type` varchar(255) DEFAULT NULL,
  `type_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tbl_job_subcat`
--

INSERT INTO `tbl_job_subcat` (`id`, `job_type_name`, `sub_cat_type`, `type_code`) VALUES
(1, 'Day Curtain', 'Curtain', NULL),
(2, 'Night Curtain', 'Curtain', NULL),
(3, 'Blackout Curtain', 'Curtain', NULL),
(4, 'Roman Blind', 'Blind', NULL),
(5, 'Roller Blind', 'Blind', NULL),
(6, 'Wooden Blind', 'Blind', NULL),
(7, 'Venician Blind', 'Blind', NULL),
(8, 'Motor Blind', 'Blind', NULL),
(9, 'Vertical Blind', 'Blind', NULL),
(10, 'Standard', 'Track', NULL),
(11, 'Heavy Duty Track', 'Track', NULL),
(12, 'Special Track', 'Track', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_job_transaction`
--

CREATE TABLE IF NOT EXISTS `tbl_job_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tbl_users_id` int(11) NOT NULL,
  `tbl_customers_id` int(11) NOT NULL,
  `job_trans_modify` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `job_trans_created` datetime NOT NULL,
  `job_trans_status` tinyint(3) DEFAULT '0',
  `job_trans_type` tinyint(2) DEFAULT '1' COMMENT '1= first time; 2=follow-up',
  PRIMARY KEY (`id`,`tbl_users_id`,`tbl_customers_id`),
  KEY `fk_tbl_job_transaction_tbl_users1_idx` (`tbl_users_id`),
  KEY `fk_tbl_job_transaction_tbl_customers1_idx` (`tbl_customers_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='												' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_job_transaction`
--

INSERT INTO `tbl_job_transaction` (`id`, `tbl_users_id`, `tbl_customers_id`, `job_trans_modify`, `job_trans_created`, `job_trans_status`, `job_trans_type`) VALUES
(1, 1, 1, '2015-04-30 05:14:28', '2015-04-30 07:14:28', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supplier`
--

CREATE TABLE IF NOT EXISTS `tbl_supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supp_co_name` varchar(255) DEFAULT NULL,
  `supp_contact_person` varchar(255) DEFAULT NULL,
  `supp_contact_num` varchar(255) DEFAULT NULL,
  `supp_email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_supplier`
--

INSERT INTO `tbl_supplier` (`id`, `supp_co_name`, `supp_contact_person`, `supp_contact_num`, `supp_email`) VALUES
(1, 'BJMP Clothing Supply', 'John Doe', '02368451', 'johnDoe@email.com'),
(2, 'Panda Curtain Supply', 'Jojo Binay', '12346487', 'jojo@email.com'),
(3, 'Pink Enterprises inc.', 'pink floyd', '123465987', 'pf@email.com'),
(4, 'BrownBox Trading', 'Brabo Estakil', '293948680', 'be@email.com'),
(5, 'Intextes Corp.', 'Jamich Dead', '22394856', 'jd@email.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaction_item`
--

CREATE TABLE IF NOT EXISTS `tbl_transaction_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tbl_job_transaction_id` int(11) NOT NULL,
  `tbl_customer_contact_id` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `asset_dtl_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_transaction_item_tbl_job_transaction1_idx` (`tbl_job_transaction_id`),
  KEY `fk_tbl_transaction_item_tbl_customers_contact1_idx` (`tbl_customer_contact_id`),
  KEY `fk_tbl_transaction_item_tbl_asset1_idx` (`asset_id`),
  KEY `fk_tbl_transaction_item_tbl_asset_dtl1_idx` (`asset_dtl_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_transaction_item`
--

INSERT INTO `tbl_transaction_item` (`id`, `tbl_job_transaction_id`, `tbl_customer_contact_id`, `asset_id`, `asset_dtl_id`) VALUES
(1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaction_type_selection`
--

CREATE TABLE IF NOT EXISTS `tbl_transaction_type_selection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `curtain_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_transaction_type_selection`
--

INSERT INTO `tbl_transaction_type_selection` (`id`, `curtain_type`) VALUES
(1, 1),
(2, 5),
(3, 11);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE IF NOT EXISTS `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_fname` varchar(255) DEFAULT NULL,
  `u_lname` varchar(255) DEFAULT NULL,
  `u_email` varchar(255) DEFAULT NULL,
  `u_contact` varchar(255) DEFAULT NULL,
  `u_address` varchar(255) DEFAULT NULL,
  `u_username` varchar(255) DEFAULT NULL,
  `u_pass` varchar(128) DEFAULT NULL,
  `u_role` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `u_fname`, `u_lname`, `u_email`, `u_contact`, `u_address`, `u_username`, `u_pass`, `u_role`) VALUES
(1, 'Marnelle', 'Apat', 'marnelle@email.com', '032651258', 'Labangon, Cebu City', 'admin', 'f410fcdf9361e931c49de545558af157f574c476b0fd1ba9fac05189fa4c9058180996f54bd29fe818c65c753dbfa42e479281a66e9b467db68be30aee2c8a53', 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_asset`
--
ALTER TABLE `tbl_asset`
  ADD CONSTRAINT `fk_tbl_asset_tbl_customers1` FOREIGN KEY (`tbl_customer_id`) REFERENCES `tbl_customers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_asset_dtl`
--
ALTER TABLE `tbl_asset_dtl`
  ADD CONSTRAINT `fk_tbl_property_dtl_tbl_asset1` FOREIGN KEY (`tbl_asset_id`) REFERENCES `tbl_asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_customers_contact`
--
ALTER TABLE `tbl_customers_contact`
  ADD CONSTRAINT `fk_tbl_customers_contact_tbl_customers1` FOREIGN KEY (`customer_id`) REFERENCES `tbl_customers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_job_classification`
--
ALTER TABLE `tbl_job_classification`
  ADD CONSTRAINT `fk_tbl_job_classification_tbl_supplier1` FOREIGN KEY (`tbl_supplier_id`) REFERENCES `tbl_supplier` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_job_transaction`
--
ALTER TABLE `tbl_job_transaction`
  ADD CONSTRAINT `fk_tbl_job_transaction_tbl_customers1` FOREIGN KEY (`tbl_customers_id`) REFERENCES `tbl_customers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_job_transaction_tbl_users1` FOREIGN KEY (`tbl_users_id`) REFERENCES `tbl_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tbl_transaction_item`
--
ALTER TABLE `tbl_transaction_item`
  ADD CONSTRAINT `fk_tbl_transaction_item_tbl_asset1` FOREIGN KEY (`asset_id`) REFERENCES `tbl_asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_transaction_item_tbl_asset_dtl1` FOREIGN KEY (`asset_dtl_id`) REFERENCES `tbl_asset_dtl` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_transaction_item_tbl_customers_contact1` FOREIGN KEY (`tbl_customer_contact_id`) REFERENCES `tbl_customers_contact` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_transaction_item_tbl_job_transaction1` FOREIGN KEY (`tbl_job_transaction_id`) REFERENCES `tbl_job_transaction` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
